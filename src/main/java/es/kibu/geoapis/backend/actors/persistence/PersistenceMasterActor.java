/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import scala.reflect.ClassTag;

public class PersistenceMasterActor extends UntypedActor {

    public static final String SERIALIZATION_MASTER_ACTOR = "serializationMasterActor";
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    public static Props props() {
        ClassTag<PersistenceMasterActor> tag = scala.reflect.ClassTag$.MODULE$.apply(PersistenceMasterActor.class);
        return Props.apply(tag);
    }

    public static class Initialize {
    }

    private ActorRef serializationMongoActor = getContext().actorOf(PersistenceMongoActor.props(), PersistenceMongoActor.SERIALIZATION_ACTOR);

    public void onReceive(Object message) throws Exception {

        serializationMongoActor.forward(message, getContext());
        /*if (message instanceof Initialize) {
            //log.info("In PersistenceMasterActor - starting ping-pong");
            serializationMongoActor.tell("ping", getSelf());
        }
        else unhandled(message);*/
    }
}