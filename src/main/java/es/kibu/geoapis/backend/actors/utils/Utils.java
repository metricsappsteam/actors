package es.kibu.geoapis.backend.actors.utils;

import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * Created by lrodriguez2002cu on 10/11/2016.
 */
public class Utils {

    public static DateTime updateMin(DateTime minDateTime, DateTime dt) {
        if (dt == null) return minDateTime;
        if (minDateTime == null) return dt;
        if (dt.isBefore(minDateTime)) return dt;
        return minDateTime;
    }


    public static DateTime updateMax(DateTime maxDateTime, DateTime dt) {
        if (dt == null) return maxDateTime;
        if (maxDateTime == null) return dt;
        if (dt.isAfter(maxDateTime)) return dt;
        return maxDateTime;
    }

    public static double updateMax(Double max, Double newDistance){
        if (max == null) return newDistance;
        if (max < newDistance) return newDistance;
        return max;
    }

    public static double updateMin(Double min, Double newDistance){
        if (min == null) return newDistance;
        if (min > newDistance) return newDistance;
        return min;
    }

    public static boolean isContained(DateTime begin, DateTime end, DateTime dateTime, boolean allowOpenInterval){
        boolean compliesTimeRestriction = false;

        Interval interval = (begin != null && end != null) ? new Interval(begin, end) : null;
        if (interval != null) {
            compliesTimeRestriction =//interval.contains(dateTime)
                    (interval.getStart().isBefore(dateTime) || (interval.getStart().isEqual(dateTime)))
                            && (interval.getEnd().isAfter(dateTime) || interval.getEnd().isEqual(dateTime));
        }
        else {
            if (allowOpenInterval) {
                //if only the end is specified
                if (begin == null && end != null) {
                    compliesTimeRestriction = end.isAfter(dateTime);
                } else {
                    //only the begining is specified
                    if (begin != null && end == null) {
                        compliesTimeRestriction = dateTime.isAfter(begin);
                    }
                    else {
                        //both endings opened
                        return true;
                    }
                }


            }

        }
        return compliesTimeRestriction;
    }
}
