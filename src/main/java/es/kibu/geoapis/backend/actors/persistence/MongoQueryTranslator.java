/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A helper for translating a query to Jongo(Mongo) query sintax
 */
public class MongoQueryTranslator implements QueryStatementTranslator {

    List<PropertyRenderInterceptor> propertyRenderInterceptors = new ArrayList<>();

    @Override
    public void addPropertyRenderInterceptor(PropertyRenderInterceptor propertyRenderInterceptor) {
        propertyRenderInterceptors.add(propertyRenderInterceptor);
    }

    interface OperatorRenderer {
        String renderQuery(Object operator, AbstractQueries.QueryValue value);
        void prepareParameters(AbstractQueries.QueryValue value, List<Object> parameterList);
    }

    static class DefaultRenderer implements OperatorRenderer{
        /*
       $eq	Matches values that are equal to a specified value.
       $gt	Matches values that are greater than a specified value.
       $gte	Matches values that are greater than or equal to a specified value.
       $lt	Matches values that are less than a specified value.
       $lte	Matches values that are less than or equal to a specified value.
       $ne	Matches all values that are not equal to a specified value.
       $in	Matches any of the values specified in an array.
       $nin	Matches none of the values specified in an array.
       */
        private String getJongoOperator(Object outOperator) {
            Object operator = outOperator;
            if (operator instanceof AbstractQueries.QueryOperator){
                operator = ((AbstractQueries.QueryOperator) operator).getOperator();
            }

            if (operator instanceof AbstractQueries.ComparisonOp){
                AbstractQueries.ComparisonOp operatorType = (AbstractQueries.ComparisonOp) operator;
                switch (operatorType){
                    case equal:
                        return "$eq";
                    case lt:
                        return "$lt";
                    case gt:
                        return "$gt";
                    case notequal:
                        return "$ne";
                    case gte:
                        return "$gte";
                    case lte:
                        return "$lte";
                    case in:
                        return "$in";
                    case nin:
                        return "$nin";
                    case elemMatch:
                        return "$elemMatch";
                }
            }
            else if (operator instanceof AbstractQueries.SpatialOp){
                AbstractQueries.SpatialOp sop = (AbstractQueries.SpatialOp) operator;
                switch(sop){
                    case within:
                        throw new RuntimeException(String.format("Operator not implemented yet (%s)", sop.name()));
                }
            }
            throw new RuntimeException(String.format("Operator not implemented (%s)", operator.toString()));
        }

        @Override
        public String renderQuery(Object operator, AbstractQueries.QueryValue value) {
            String jongoOperator = getJongoOperator(operator);
            return String.format("{%s : #}", jongoOperator);
        }

        @Override
        public void prepareParameters(AbstractQueries.QueryValue parameterValue, List<Object> parameterList) {
            Object preparedValue =  prepareValue(parameterValue);
            parameterList.add(preparedValue);
        }

        public Object prepareValue(AbstractQueries.QueryValue value) {
            Serializable innerValue = value.getValue();
            return prepareInnerValue(innerValue);
        }

        public Object prepareInnerValue(Object innerValue) {
            if (innerValue instanceof String) {
                return quote((String) innerValue);
            }
            else if (innerValue instanceof DateTime){
                DateTime dt = (DateTime) innerValue;
                return dt.toDate();
            }
            else if (innerValue instanceof Date){
                return innerValue;
            }
            else {
                return  innerValue.toString();
            }
        }
    }

    static class QueryFilterValueRenderer extends DefaultRenderer {

        QueryStatementTranslator translator = new MongoQueryTranslator();

        public QueryFilterValueRenderer(/*QueryStatementTranslator translator1*/) {
            //this.translator = translator;
            translator.setRootProperty("");
        }

        @Override
        public String renderQuery(Object operator, AbstractQueries.QueryValue value) {
            String  query = "";
            if (value.getValue() instanceof AbstractQueries.QueryFilter) {
                Object innerValue = value.getValue();
                AbstractQueries.QueryFilter filter = (AbstractQueries.QueryFilter)  innerValue;
                ArrayList<Object> parameters = new ArrayList<>();
                String filterRender = ((MongoQueryTranslator)translator).createFilter(filter, parameters);
                query = String.format("{%s : {%s}}", super.getJongoOperator(operator), filterRender);
            }
            else {
                query = super.renderQuery(operator, value);
            }

            return query;
        }

        @Override
        public Object prepareInnerValue(Object innerValue) {
            return super.prepareInnerValue(innerValue);
        }



        @Override
        public void prepareParameters(AbstractQueries.QueryValue parameterValue, List<Object> parameterList) {
            Object preparedValue;
            if (parameterValue.getValue() instanceof AbstractQueries.QueryFilter){
                AbstractQueries.QueryFilter filter  = (AbstractQueries.QueryFilter) parameterValue.getValue();
                preparedValue = prepareValue(filter.getValue());
            }
            else {
                preparedValue =  prepareValue(parameterValue);
            }
            parameterList.add(preparedValue);
        }

    }

    static class RangeOperatorRenderer extends DefaultRenderer {

        public Object[] prepareValue(AbstractQueries.QueryValue queryValue) {
            Object [] result = new Object[2];
            Object value = queryValue.getValue();
            if (value instanceof Array) {
                Array arr = ((Array) value);
                if (Array.getLength(arr)>=2){
                    Object rangeLow = Array.get(arr, 0);
                    Object rangeHigh = Array.get(arr, 1);

                    result[0] = super.prepareInnerValue(rangeLow);
                    result[1] = super.prepareInnerValue(rangeHigh);
                    return result;

                }

            }
            else if (value instanceof List) {
                if (((List) value).size() >= 2) {
                    result[0] = super.prepareInnerValue(((List) value).get(0));
                    result[1] = super.prepareInnerValue(((List) value).get(0));
                    return result;
                }
            }

            //return null;
            throw new RuntimeException("The content of the Query value cant be translated as parameters. " +
                    "For ranges use an array or a list");
        }

        @Override
        public void prepareParameters(AbstractQueries.QueryValue parameterValue, List<Object> parameterList) {
            Object[] rangeValues = prepareValue(parameterValue);
            parameterList.add(rangeValues[0]);
            parameterList.add(rangeValues[1]);
        }

        @Override
        public String renderQuery(Object operator, AbstractQueries.QueryValue value) {
            if (operator instanceof AbstractQueries.RangeOperator) {
               return   "{ $gte: #, $lte: #}";
            }
            return null;
        }

    }

    static class WithIDOperatorRenderer extends DefaultRenderer{
        @Override
        public String renderQuery(Object operator, AbstractQueries.QueryValue value) {
            if (operator instanceof AbstractQueries.ComparisonOperators &&
                    ((AbstractQueries.ComparisonOperators) operator).getOperator() == AbstractQueries.ComparisonOp.withId ) {
                return   "{$oid: # }";
            }
            return null;
        }
    }


    static class RegExOperatorRenderer extends DefaultRenderer {
        @Override
        public String renderQuery(Object operator, AbstractQueries.QueryValue value) {
            String options = "";
            if (operator instanceof AbstractQueries.REG_EX) {
                String opOptions = ((AbstractQueries.REG_EX) operator).getOptions();
                options = String.format(", $options: %s", quote(opOptions));
            }
            //return options + " $regex";
            return "{$regex : #"+ options +" }";
        }

        @Override
        public void prepareParameters(AbstractQueries.QueryValue parameterValue, List<Object> parameterList) {
            super.prepareParameters(parameterValue, parameterList);
        }
    }

    private OperatorRenderer getOperatorRenderer(Object operator) {
        Object tempOperator = operator ;
        if (tempOperator instanceof AbstractQueries.QueryOperator){
            tempOperator = ((AbstractQueries.QueryOperator) tempOperator).getOperator();
        }

        if (tempOperator instanceof AbstractQueries.ComparisonOp) {

            if (tempOperator.equals(AbstractQueries.ComparisonOp.range)){

                return new RangeOperatorRenderer();
            }
            if (tempOperator.equals(AbstractQueries.ComparisonOp.regex)){
                return new RegExOperatorRenderer();
            }

            if (tempOperator.equals(AbstractQueries.ComparisonOp.withId)){
                return new WithIDOperatorRenderer();
            }

            if (tempOperator.equals(AbstractQueries.ComparisonOp.elemMatch)){
                return new QueryFilterValueRenderer(/*this*/);
            }

            return new DefaultRenderer();
        }

        return null;
    }

    String rootProperty;

    public String getRootProperty() {
        return rootProperty;
    }

    public void setRootProperty(String rootProperty) {
        this.rootProperty = rootProperty;
    }

    String getPropertyTemplate(String property) {
        String template = renderTemplateWithInterceptors(property);
        if (template == null){

            return ((rootProperty.isEmpty())? "": rootProperty + ".") +"%s";
        }
        else return template;
    }

    private String renderTemplateWithInterceptors(String property) {
        for (PropertyRenderInterceptor propertyRenderInterceptor : propertyRenderInterceptors) {
            if (propertyRenderInterceptor instanceof PropertyTemplateRenderInterceptor){
                String template = ((PropertyTemplateRenderInterceptor) propertyRenderInterceptor).renderPropertyTemplate(this, property);
                if (template!= null) {
                    return template;
                }
            }
        }
        return null;
    }

    public String createFilter(AbstractQueries.QueryFilter filter, List<Object> parameters){
        String result = "";
        String propertyName = filter.getProperty().getPropertyName();
        AbstractQueries.QueryValue value = filter.getValue();

        Object operator = filter.getOperator();
        OperatorRenderer renderer = getOperatorRenderer(operator);

        //render the params and the query
        renderer.prepareParameters(value, parameters);
        String operatorStringRender = renderer.renderQuery(operator, value);

        //render with the property template plus the property and so on
        result = String.format(getPropertyTemplate(propertyName) + ": %s", propertyName, operatorStringRender);
        return result;
     }


    static boolean forJongo() {
        return true;
    }

    public static String quote(String value) {
        return forJongo()? value : "'"+ value + "'";
    }

    private String createFilters(AbstractQueries.Filters filters, List<Object> parameters ){
        String sep = "";
        String result= "";
        for (AbstractQueries.QueryFilter filter : filters.filters) {
            result+= sep + createFilter(filter, parameters) ;
            sep = ", ";
        }
        //$query: {}
        //return !result.isEmpty()? String.format(" $query: { %s} ", result): "";
        return !result.isEmpty()? String.format("{ %s }", result): "";
    }

    public class DefaultQueryTranslatorResult implements QueryTranslatorResult{

        QueryDef query;
        int limit = -1;
        int skip = -1;

        boolean isCountValue;
        private List<AbstractQueries.QuerySort> sorts;

        public void setQuery(QueryDef query) {
            this.query = query;
        }

        @Override
        public QueryDef getQuery() {
            return query;
        }

        @Override
        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        @Override
        public int getSkip() {
            return skip;
        }

        public void setSkip(int skip) {
            this.skip = skip;
        }

        @Override
        public boolean isCount() {
            return isCountValue;
        }

        public void setIsCount(boolean isCount) {
            this.isCountValue = isCount;
        }

        public void setSorts(List<AbstractQueries.QuerySort> sorts) {
            this.sorts = sorts;
        }

        public List<AbstractQueries.QuerySort> getSorts() {
            return sorts;
        }
    }

   /* private String withCommaIfNotEmpty(String part, boolean andMore) {
        return part.isEmpty()? "" : ((andMore)? part + ",": part);
    }*/


    AbstractQueries.Query currentQuery;

    public AbstractQueries.Query getCurrentQuery() {
        return currentQuery;
    }


    @Override
    public QueryTranslatorResult translateQuery(AbstractQueries.Query query) {

        currentQuery = query;

        List<Object> parameters = new ArrayList<>();

        List<String> partList = new ArrayList<>();

        String filters = createFilters(query.getFilters(), parameters);

        //for the full add the filters with the data
        addIfNotEmpty(partList, !filters.isEmpty() ? String.format(" $query: %s ", filters) : "");

        String sort = createSort(query.getSorts());

        query.getLimit();

        addIfNotEmpty(partList, sort);

        String skip = getSkip(query.getSkip());
        addIfNotEmpty(partList, skip);

        String limit = getLimit(query.getLimit());
        addIfNotEmpty(partList, limit);


        String fullQuery = "{";
        String sep = "";
        for (String part : partList) {
            fullQuery += sep + part;
            sep = ",";
        }

        fullQuery += "}";

        DefaultQueryTranslatorResult result = new DefaultQueryTranslatorResult();

        result.setLimit(query.getLimit());
        result.setSkip(query.getSkip());
        result.setSorts(query.getSorts());

        QueryDef queryDef = new DefaultQueryDef();
        queryDef.setFiltersPart(filters);
        queryDef.setOrderPart(sort);
        queryDef.setSkipPart(skip);
        queryDef.setLimitPart(limit);
        queryDef.setFullQuery(fullQuery);
        queryDef.setQueryParams(parameters);

        result.setQuery(queryDef);

        return result;
    }

    private void addIfNotEmpty(List<String> partList, String filters) {
        if (!filters.isEmpty()){
            partList.add(filters);
        }
    }

    private String getLimit(int limit){

        //String result = (limit == -1)? "": String.format("$maxScan: %s", Integer.toString(limit));
        //String result = (limit <= 0 )? "": String.format("$limit: %s", Integer.toString(limit));
        String result = (limit <= 0 )? "": String.format("$maxScan: %s", Integer.toString(limit));
        return  result;
    }

    private String getSkip(int skip){
        String result = (skip <= 0 )? "": String.format("$skip: %s", Integer.toString(skip));
        return  result;
    }

    private String createSort(List<AbstractQueries.QuerySort> sorts) {
        String result = "";
        String sep = "";

        for (AbstractQueries.QuerySort sort : sorts) {
            String propertyName = sort.getProperty().getPropertyName();
            result+= sep + String.format(getPropertyTemplate(propertyName)+ ": %s", propertyName,
                    (sort.getDirection() == AbstractQueries.SortDirection.ASC)? "1":"0") ;
            sep = ", ";
        }
        //{$query: {}, $orderby: {name: 1}}
        return (!result.isEmpty())?String.format("$orderBy: {%s}", result) : "";
    }


}
