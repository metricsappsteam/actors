/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors;

import java.io.Serializable;

/**
 * Created by lrodr_000 on 22/12/2015.
 */
public class GeoUtils {
    public static final int R = 6371000;

/*
    Taken from:
    http://www.movable-type.co.uk/scripts/latlong.html

    var R = 6371000; // metres
    var φ1 = lat1.toRadians();
    var φ2 = lat2.toRadians();
    var Δφ = (lat2-lat1).toRadians();
    var Δλ = (lon2-lon1).toRadians();

    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
                    Math.sin(Δλ/2) * Math.sin(Δλ/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    var d = R * c;*/


    public static double harvesineDistance(double latitude1, double longitude1, double latitude2, double longitude2 ){
        double φ1 = Math.toRadians(latitude1);
        double φ2 = Math.toRadians(latitude2);
        double Δφ = Math.toRadians(latitude2-latitude1);
        double Δλ = Math.toRadians(longitude2-longitude1);

        double a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                        Math.sin(Δλ/2) * Math.sin(Δλ/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        double d = R * c;
        return d;
    }

    public static boolean insideRadiusFromPoint(double latitudeToCheck, double longitudeToCheck, LatLongRadius latLongRadius){
        return GeoUtils.insideRadiusFromPoint(latitudeToCheck, longitudeToCheck,
                latLongRadius.getLatitude(), latLongRadius.getLongitude(), latLongRadius.getRadius());
    }


    public static boolean insideRadiusFromPoint(double latitudeToCheck, double longitudeToCheck, double latitude, double longitude, double radiusInMeters){
        double distance = harvesineDistance(latitudeToCheck, longitudeToCheck, latitude, longitude);
        return distance < radiusInMeters;
    }

    public static class LatLongRadius implements Serializable{

        Double latitude;
        Double longitude;
        Double radius;

        public LatLongRadius(double latitude, double longitude, double radius) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
        }

        public LatLongRadius() {
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getRadius() {
            return radius;
        }

        public void setRadius(double radius) {
            this.radius = radius;
        }

        public void setPositionAndRadius(double latitude,
                                         double longitude,
                                         double radius){
            this.setLatitude(latitude);
            this.setLongitude(longitude);
            this.setRadius(radius);
        }

        public boolean isUnset(){
            return latitude != null &&  longitude != null &&  radius!= null;
        }

        @Override
        public String toString() {
            return "LatLongRadius{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", radius=" + radius +
                    '}';
        }
    }

}
