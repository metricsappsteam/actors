/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.recipients;


/**
 * Created by lrodriguez2002cu on 25/04/2016.
 */
public abstract class DefaultRecipient implements Recipient{

    /**
     * The platform name the client is running on, for example,
     * "iPhone OS 4.0.1", "Palm WebOS", "Android Donut", "ruby" are possible
     * values.
     */
    public String platform; // Required
    /**
     * The hardware version the client is running. For example, an iPhone
     * 3GS returns the string "iPhone2,1". See this page for more details.
     */
    public String hardware;// Required

    /**
     * For official Geoloqi clients, this value should be set to "Geoloqi".
     * For other clients, set a descriptive name for your application.
     */
    public String name; // Required

    /**
     * The version number of the application sending data. This may be "1.0"
     * for the official iPhone app, or you can use your own versioning
     * scheme such as a date stamp like "20100731".
     */
    public String version; // Required

    @Override
    public String getPlatform() {
        return platform;
    }

    @Override
    public String getHardware() {
        return hardware;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public DefaultRecipient(String platform) {
        this.platform = platform;
    }
}
