/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.senders;

import com.google.gson.Gson;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.results.HttpNotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.Serializable;
import java.net.URL;

/**
 * Created by lrodriguez2002cu on 25/04/2016.
 */

public class HttpNotificationSender extends BaseNotificationSender {

    private static final String USER_AGENT = "FencesServer";
    public static Logger logger = LoggerFactory.getLogger(HttpNotificationSender.class);

    private URL url;

    @Override
    public NotificationResult send(NotificationMessage notificationMessage) {

        NotificationResult notificationResult = null;
        try {
            int responseCode = sendMessage(notificationMessage);
            boolean success = responseCode == 200;
            String targetUrl = notificationMessage.getTo().getTarget();
            if (targetUrl == null) {
                throw new RuntimeException(String.format("Invalid target, target is null (%s)",  HttpNotificationSender.class.getName()));
            }
            url = new URL(targetUrl);
            notificationResult = new HttpNotificationResult(success);
            Serializable resultData = !success ? new HttpNotificationResult.HttpNotificationError(responseCode) : responseCode;

            notificationResult.setData(resultData);
        } catch (Exception e) {
            notificationResult = new HttpNotificationResult(false);
            notificationResult.setData(new HttpNotificationResult.HttpNotificationError(e.getMessage()));
        }
        return notificationResult;
    }

    public HttpNotificationSender(/*NotificationMessage message*/) {

    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    // HTTP POST request
    public int sendMessage(NotificationMessage notificationMessage) throws Exception {

        String payload = toJson(notificationMessage);

        /*String url = "https://selfsolve.apple.com/wcResults.do";
        URL obj = new URL(url);*/

        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(payload);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        logger.debug(String.format("response code from service %s", responseCode));
        return responseCode;
    }

    private  String toJson(NotificationMessage message) {

        return message.getData();
        // throw  new RuntimeException("Json Conversion is not defined!!");
    }


}
