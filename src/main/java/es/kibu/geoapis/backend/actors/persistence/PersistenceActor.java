/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import es.kibu.geoapis.backend.actors.messages.*;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.*;
import es.kibu.geoapis.backend.configuration.Settings;
import es.kibu.geoapis.backend.configuration.SettingsImpl;

import java.io.Serializable;
import java.util.List;

import static es.kibu.geoapis.backend.actors.persistence.PersistenceMessageUtils.queryWithId;

/**
 * Created by lrodriguez2002cu on 04/12/2015.
 * This class is an abstraction of the concept of an actor in charge of serializing information (i.e. to DB).
 * The classes used does not have any dependency with an specific db vendor. This are more like instructions
 * about what to persist, read, update, delete without specifying the how. It also includes the response types.
 */
@SuppressWarnings("Duplicates")
public abstract class PersistenceActor extends UntypedActor {
    //the settings of the application
    final SettingsImpl settings = Settings.SettingsProvider.get(getContext().system());

    public static final String APPLICATION_ID = "applicationId";
    public static final String USER_ID = "userId";

    //the log
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    protected abstract QueryResult doDelete(Query query) throws Exception;

    protected abstract QueryResult doUpdate(Query query) throws Exception;

    protected abstract QueryResult doSelection(Query query) throws Exception;

    protected abstract QueryResult doInsertion(Query query) throws Exception;

    public Query queryFromCommand(CommandMessage<ContextInfo> commandMessage) {
        CRUDCommands cmd = (CRUDCommands) commandMessage.getCommand();

        Query resultQuery = null;

        //extract the target it refers to
        assert commandMessage.getData() instanceof ContextInfo;

        String target = null;

        //set the target
        //setCollection(target);

        //set the entityid
        EntityId entityId = null;
        //setId(entityId);

        //set the application id
        ApplicationId applicationId = null;

        UserId userId= null;
        //setApplication(applicationId);

        //set the entity
        Serializable entity = null;
        //setEntity(entity);

        //set the result class
        Class resultClass = null;


        if (commandMessage.getData() instanceof ContextInfo) {
            ContextInfo ctx = commandMessage.getData();

            target = ctx.getCommand().getTarget();

            //set the target
            //setCollection(target);

            //set the entityid
            entityId = ctx.getEntityId();
            //setId(entityId);

            //set the application id
            applicationId = ctx.getApplicationId();
            //setApplication(applicationId);

            userId = ctx.getUserId();

            //set the entity
            entity = ctx.getEntity();
            //setEntity(entity);

            //set the result class
            resultClass = ctx.getResultClass();
            //setResultClass(resultClass);
            /*try {

                Uri uri = ctx.getUri();
                String query = uri.rawQueryString().isDefined() ? uri.rawQueryString().get() : "";
                String fragment = uri.fragment().isDefined() ? uri.fragment().get() : "";

                String decode = null;
                RawUri ruri = UriDecoder.decodeUri(uri.path(), query, fragment, 0);
                UriParserParser parserParser = new UriParserParser(null);


            } catch (UriParserSyntaxException e) {
                e.printStackTrace();
            }*/

        }

        QueryFilter applicationFilter = PersistenceMessageUtils.applicationIdFilter(applicationId);
        //new QueryFilter(new EntityProperty(APPLICATION_ID), ComparisonOperators.EQUAL, new QueryValue(applicationId));


        //set the type
        switch (cmd) {

            case CREATE:
                resultQuery = new InsertQuery(entity, target);
                if (applicationFilter != null){
                    resultQuery.withFilter(applicationFilter);
                }
                addUserIdFilterIfAppropriate(resultQuery, userId);
                break;
            case READ:
                resultQuery = new SelectQuery(resultClass, target);
                resultQuery.withFilter(applicationFilter);
                queryWithId(resultQuery, entityId);
                addUserIdFilterIfAppropriate(resultQuery, userId);
                break;
            case UPDATE:
                resultQuery = new UpdateQuery(entity, target);
                resultQuery.withFilter(applicationFilter);
                queryWithId(resultQuery, entityId);
                addUserIdFilterIfAppropriate(resultQuery, userId);
                break;
            case DELETE:
                resultQuery = new DeleteQuery(entityId, target);
                addUserIdFilterIfAppropriate(resultQuery, userId);
                resultQuery.withFilter(applicationFilter);
                queryWithId(resultQuery, entityId);
                break;
        }

        return resultQuery;

    }

    private Query addUserIdFilterIfAppropriate(Query query, UserId userId){
        return PersistenceMessageUtils.queryWithUserId(query, userId);
    }


    /**
     * Handles the message by identifying the operation and performing
     * the operation indicated in the message
     * todo: move this to the parent class
     * @param commandMessage the message comming from the outside world
     * @return the result of performing the query specified
     */
     QueryResult handleMessage(CommandMessage commandMessage) {
        if (commandMessage.getCommand() instanceof CRUDCommands) {
            //CRUDCommands cmd = (CRUDCommands) commandMessage.getCommand();
            AbstractQueries.Query query = queryFromCommand(commandMessage);
            return handleQuery(query);

        }
        return new DBExceptionResult("Unknown command arrived to serialization actor");
    }

    /**
     * Handles a query received by the actor.
     * @param query the query to be handled
     * @return a query result
     */
    QueryResult handleQuery(Query query) {
        QueryResult result;
        try {
            switch (query.getQueryType()) {
                case INSERT:
                    result = doInsertion(query);
                    break;
                case SELECT:
                    result = doSelection(query);
                    break;
                case UPDATE:
                    result = doUpdate(query);
                    break;
                case DELETE:
                    result = doDelete(query);
                    break;
                default:
                    result = new DBExceptionResult("Unknown command arrived to serialization actor");
            }
            return result;
        } catch (Exception e) {
            return new DBExceptionResult(e);
        }
    }


    String getFilterProperty(Query query, String propertyName){
        QueryFilter filterByName = query.getFilters().getFilterByName(propertyName);
        if (filterByName != null){
            return filterByName.getValue().getValueAsString();
        }
        return null;
    }


    public static abstract class QueryResult implements Serializable {
        public abstract Result.ResultType getResultType();
    }


    public static class DBWriteResult extends QueryResult {
        String id;

        public String getId() {
            return id;
        }

        public DBWriteResult(String id) {
            this.id = id;
        }

        @Override
        public Result.ResultType getResultType() {
            return Result.ResultType.ok;
        }
    }

    public static class DBUpdateResult extends QueryResult {

        String id;
        int modified_objects;
        boolean existed;

        public String getId() {
            return id;
        }

        public int getModified_objects() {
            return modified_objects;
        }

        public boolean isExisted() {
            return existed;
        }

        public DBUpdateResult(String id, boolean existed, int numberMmodified) {
            this.id = id;
            this.existed = existed;
            this.modified_objects = numberMmodified;
        }

        public int getModifiedObjects(){
            return modified_objects;
        }

        @Override
        public Result.ResultType getResultType() {
            return (existed)? Result.ResultType.ok : Result.ResultType.error;
        }
    }


    public static class DBDeleteResult extends DBWriteResult {
        boolean deleted;

        public boolean isDeleted() {
            return deleted;
        }

        public DBDeleteResult(String id, boolean deleted) {
            super(id);
            this.deleted = deleted;
        }

        @Override
        public Result.ResultType getResultType() {
            return (deleted) ? Result.ResultType.ok : Result.ResultType.error;
        }
    }


    public static class DBReadResult extends QueryResult {
        List<Object> resultset;

        public List<Object> getResultset() {
            return resultset;
        }

        public DBReadResult(List<Object> selectionResult) {
            this.resultset = selectionResult;
        }

        @Override
        public Result.ResultType getResultType() {
            return Result.ResultType.ok;
        }
    }

    public static class DBExceptionResult extends QueryResult {
        String message;

        public String getMessage() {
            return message;
        }

        public DBExceptionResult(Exception e) {
            this.message = e.getMessage();
        }

        public DBExceptionResult(String message) {
            this.message = message;
        }

        @Override
        public Result.ResultType getResultType() {
            return Result.ResultType.error;
        }
    }


    protected Result<QueryResult> createResultMsg(QueryResult result) {
        Result<QueryResult> queryResultResult;
        if (result instanceof DBExceptionResult) {
            queryResultResult = new Result<>(result.getResultType(), ((DBExceptionResult) result).getMessage(), result);
        }
        else queryResultResult = new Result<>(result.getResultType(), result);
        return queryResultResult;
    }



}
