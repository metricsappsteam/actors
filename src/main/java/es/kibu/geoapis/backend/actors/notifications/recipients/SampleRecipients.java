/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.recipients;

import java.util.List;

import static es.kibu.geoapis.backend.actors.notifications.recipients.Platforms.ANDROID;

/**
 * Created by lrodr_000 on 25/04/2016.
 */
public class SampleRecipients {

    public Recipient createSampleAndroidRecipient(final String clientUUID){

        return new DefaultRecipient(ANDROID) {
            @Override
            public String getTarget() {
                return clientUUID;
            }

            @Override
            public List<String> getCategories() {
                return null;
            }
        };
    }
}
