/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.messages;



import java.io.Serializable;
import java.net.URI;

/**
 * Information that can be attached to the Messages in between the UntypedActors
 */
public class ContextInfo implements Serializable {


    public enum HttpMethod {
      GET, POST, PUT, DELETE
    }

    private ApplicationId applicationId;
    private UserId userId;
    private URI uri;
    private HttpMethod method;
    private Serializable entity;
    private EntityId entityId;
    private Class resultClass;

    public ContextInfo(ContextInfo otherCtx) {
        this.applicationId = otherCtx.getApplicationId();
        this.uri = otherCtx.getUri();
        this.method = otherCtx.getMethod();
        this.entity = otherCtx.getEntity();
        this.entityId = otherCtx.getEntityId();
        this.resultClass = otherCtx.getResultClass();
        this.userId = otherCtx.getUserId();
    }

    public ContextInfo clone() {
        return new ContextInfo(this);
    }

    public Class getResultClass() {
        return resultClass;
    }

    public ApplicationId getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(ApplicationId applicationId) {
        this.applicationId = applicationId;
    }

    public Serializable getEntity() {
        return entity;
    }

    public void setEntity(Serializable entity) {
        this.entity = entity;
    }

    public EntityId getEntityId() {
        return entityId;
    }

    public void setEntityId(EntityId entityId) {
        this.entityId = entityId;
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public ContextInfo(ApplicationId applicationId, URI uri, HttpMethod method) {
        this.uri = uri;
        this.method = method;
        this.applicationId = applicationId;
    }

    /**
     * For using with the post method, in which an entity is provided
     *
     * @param uri    the uri of the request
     * @param method the method
     * @param entity the entity associated (if the case)
     */
    public ContextInfo(ApplicationId applicationId, URI uri, HttpMethod method, Serializable entity) {
        this(applicationId, uri, method);
        this.entity = entity;
    }

    public ContextInfo(ApplicationId applicationId, URI uri, HttpMethod method, EntityId entityId, Class resultClass) {
        this.applicationId = applicationId;
        this.uri = uri;
        this.method = method;
        this.entityId = entityId;
        this.resultClass = resultClass;
    }

    /**
     * For using with the post method, in which an entity is provided
     *
     * @param uri      the uri of the request
     * @param method   the method
     * @param entity   the entity associated (if the case)
     * @param entityId the id of the entity to be retrieved or modified
     */

    public ContextInfo(ApplicationId applicationId, URI uri, HttpMethod method, Serializable entity, EntityId entityId) {
        this(applicationId, uri, method, entity);
        this.entityId = entityId;
    }

    /**
     * Tells if an id has been specified to the request.
     *
     * @return true if an id has been especified
     */
    public boolean hasId() {
        return (this.entityId != null) && !this.entityId.isUnassigned();
    }

    /**
     * Returns the command
     *
     * @return
     */

    ActionCommand command = null;

    public ActionCommand getCommand() {
        return (command == null) ? ActionCommand.fromContext(this) : command;
    }

    public void setCommand(ActionCommand actionCommand) {
        this.command = actionCommand;
    }

    public static class ActionCommand implements Command {

        String target;

        public String getTarget() {
            return target;
        }

        public String getAction() {
            return action;
        }

        String action;

        public ActionCommand(String target, String command) {
            this.target = target;
            this.action = command;
        }

        public ActionCommand() {
        }

        public static ActionCommand fromContext(ContextInfo info) {

            String uri = info.getUri().getPath();
            String[] seq = uri.split("/");

            int correction = 0;

            if (info.hasId()) {
                correction = 1;
            }

            int actionIndex = seq.length - 1 - correction;
            int targetIndex = seq.length - 2 - correction;

            String action = seq[actionIndex];
            String target = seq[targetIndex];
            return new ActionCommand(target, action);
        }

        @Override
        public String verb() {
            return action;
        }
    }

    @Override
    public String toString() {
        return "ContextInfo{" +
                "uri='" + uri.toString() + '\'' +
                ", method=" + method +
                ", entity=" + entity +
                ", entityId='" + entityId + '\'' +
                ", applicationId='" + applicationId + '\'' +
                '}';
    }
}
