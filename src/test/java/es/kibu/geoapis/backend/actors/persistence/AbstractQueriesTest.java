/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import es.kibu.geoapis.backend.actors.messages.EntityId;
/*
import es.kibu.geoapis.geofences.client.location.LocationUpdateParams;
import es.kibu.geoapis.geofences.client.triggers.TriggerCreateParams;
*/
import junit.framework.TestCase;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 14/12/2015.
 */
public class AbstractQueriesTest extends TestCase{

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public static AbstractQueries.QueryStatement getQuerySelectStatement2Filters() {
        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(Contact.class, "trigger");
        statement.withFilter("text", AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue("invoke"));
        statement.withFilter("name", AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue("url"))
        .sortDescendingBy("name").withProjection(new AbstractQueries.EntityProperty("name")).build();
        return statement;
    }

    public static AbstractQueries.Query getQuerySelectStatementForAppIdOnLocations(String appId, Class clazz) {
        AbstractQueries.Query statement = new AbstractQueries.SelectQuery(clazz/*LocationUpdateParams.class*/, "location");
        statement.withFilter("authTypeRequired", AbstractQueries.ComparisonOperators.EQUAL, "ApplicationToken");
        statement.withFilter(PersistenceMessageUtils.APPLICATION_ID, AbstractQueries.ComparisonOperators.EQUAL, appId)
                .sortDescendingBy("authTypeRequired").build();
        return statement;
    }


    public static AbstractQueries.Query getQuerySelectStatementForAppIdOnLocationsOnElements(String appId, Class clazz) {
        AbstractQueries.Query statement = new AbstractQueries.SelectQuery(clazz/*LocationUpdateParams.class*/, "location");
        statement.withFilter(PersistenceMessageUtils.APPLICATION_ID, AbstractQueries.ComparisonOperators.EQUAL, appId);
        statement.withFilter("points", AbstractQueries.ComparisonOperators.ELEM_MATCH,
                new AbstractQueries.QueryValue(new AbstractQueries.QueryFilter("uuid", AbstractQueries.ComparisonOperators.EQUAL, "uuid765")))
                .build();
        return statement;
    }


    public static AbstractQueries.QueryStatement getQuerySelectStatementRangeFilterAndEqual(Class clazz/*TriggerCreateParams.class*/ ) {

        DateTime minDate = DateTime.now();
        DateTime maxDate = DateTime.now().plusDays(5);

        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(clazz, "trigger");
        List<DateTime> ranges = new ArrayList<>();
        ranges.add(minDate);
        ranges.add(maxDate);
        statement.withFilter("date_to", new AbstractQueries.RangeOperator(), new AbstractQueries.QueryValue((Serializable) ranges));
        statement.withFilter("url", AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue("url"))
                .sortBy("name", AbstractQueries.SortDirection.DESC).withProjection(new AbstractQueries.EntityProperty("name")).build();

        return statement;
    }
    public static AbstractQueries.QueryStatement getQuerySelectStatementRangeFilter(Class<Object> resultClass) {

        DateTime minDate = DateTime.now();
        DateTime maxDate = DateTime.now().plusDays(5);

        List<DateTime> ranges = new ArrayList<>();

        ranges.add(minDate);
        ranges.add(maxDate);

        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(resultClass, "trigger");
        statement.withFilter("date_to", new AbstractQueries.RangeOperator(), new AbstractQueries.QueryValue((Serializable) ranges));
        statement.sortBy("date_to", AbstractQueries.SortDirection.DESC).withProjection(new AbstractQueries.EntityProperty("name")).build();

        return statement;
    }


    public static AbstractQueries.QueryStatement getQuerySelectStatementRangeFilterAndIdFilter(String appId, String target, Class resultClass) {

        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(resultClass, target);
        DateTime minDate = DateTime.now();
        DateTime maxDate = DateTime.now().plusDays(5);

        List<DateTime> ranges = new ArrayList<>();
        ranges.add(minDate);
        ranges.add(maxDate);
        statement.withFilter("date_to", new AbstractQueries.RangeOperator(), new AbstractQueries.QueryValue((Serializable) ranges));
        statement.sortBy("date_to", AbstractQueries.SortDirection.DESC);
        statement.withFilter("_id", AbstractQueries.ComparisonOperators.WITH_ID, "56b9db15b8c42800e4986c26");
        statement.withFilter(PersistenceMessageUtils.APPLICATION_ID, AbstractQueries.ComparisonOperators.EQUAL, appId);

        return statement;
    }


    public static AbstractQueries.QueryStatement simpleSelectQuery(String target, Class resultClass) {
        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(resultClass, target);
        return statement;
    }
    /*Dummy class for using in the tests*/
    class Contact {

    }

    @Test
    public void testAbstractQueries(){

        AbstractQueries.QueryStatement statement = getQueryStatement();

        assertTrue(statement.getFilters().size() ==1);
        assertTrue(statement.getSorts().size() ==1);
        assertTrue(statement.getProjections().size() ==1);


    }

    public static AbstractQueries.QueryStatement getQueryStatement() {
        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(Contact.class, "contact");
        statement.withFilter("name", AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue("luis"))
                .sortDescendingBy("name").withProjection(new AbstractQueries.EntityProperty("name")).build();
        return statement;
    }

    public void testAbstractQueries1(){

        String propertyName = "filter_property";
        String sortingProperty = "sortingPropertyName";
        String projectionProperty = "proyectionPropertyName";
        String propertyValue = "filterPropertyValue";
        AbstractQueries.Query statement = getSampleSelectQuery(propertyName, sortingProperty, projectionProperty, propertyValue);


        //filter
        AbstractQueries.QueryFilter queryFilter = statement.getFilters().get(0);

        assertTrue(statement.getFilters().size() ==1 && queryFilter.getProperty().getPropertyName().equalsIgnoreCase(propertyName)
                && queryFilter.getValue().getValueAsString().equalsIgnoreCase(propertyValue));

        //sorting
        List<AbstractQueries.QuerySort> sorts = statement.getSorts();
        AbstractQueries.QuerySort querySort = sorts.get(0);

        assertTrue(sorts.size() ==1 &&  querySort.getProperty().getPropertyName().equalsIgnoreCase(sortingProperty) &&
                querySort.getDirection()== AbstractQueries.SortDirection.DESC);

        //projection
        List<AbstractQueries.QueryProjection> projections = statement.getProjections();
        AbstractQueries.QueryProjection queryProjection = projections.get(0);

        assertTrue(projections.size() ==1);
        assertTrue(projections.size() ==1 && queryProjection.getProperties().get(0).getPropertyName().equalsIgnoreCase(projectionProperty));

        assertTrue(statement.getQueryType() == AbstractQueries.QueryType.SELECT);
        assertTrue(statement.getTarget().getTargetName().equalsIgnoreCase("contact"));

    }

    public static AbstractQueries.Query getSampleSelectQuery(String propertyName, String sortingProperty, String projectionProperty, String propertyValue) {
        //statement
        AbstractQueries.Query statement = new AbstractQueries.SelectQuery(Contact.class, "contact");
        statement.withFilter(propertyName, AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue(propertyValue))
                .sortDescendingBy(sortingProperty).withProjection(new AbstractQueries.EntityProperty(projectionProperty)).build();
        return statement;
    }


    public void testUpdateQueries(){

        String propertyName = "filter_property";
        String sortingProperty = "sortingPropertyName";
        String projectionProperty = "proyectionPropertyName";
        String propertyValue = "filterPropertyValue";


        //statement
        AbstractQueries.Query statement = new AbstractQueries.UpdateQuery(Contact.class, "contact");
        statement.withFilter(propertyName, AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue(propertyValue))
                .sortDescendingBy(sortingProperty).withProjection(new AbstractQueries.EntityProperty(projectionProperty)).build();

        //filter
        AbstractQueries.QueryFilter queryFilter = statement.getFilters().get(0);

        assertTrue(statement.getFilters().size() ==1 && queryFilter.getProperty().getPropertyName().equalsIgnoreCase(propertyName)
                && queryFilter.getValue().getValueAsString().equalsIgnoreCase(propertyValue));

        //sorting
        List<AbstractQueries.QuerySort> sorts = statement.getSorts();
        AbstractQueries.QuerySort querySort = sorts.get(0);

        assertTrue(sorts.size() ==1 &&  querySort.getProperty().getPropertyName().equalsIgnoreCase(sortingProperty) &&
                querySort.getDirection() == AbstractQueries.SortDirection.DESC);

        //projection
        List<AbstractQueries.QueryProjection> projections = statement.getProjections();
        AbstractQueries.QueryProjection queryProjection = projections.get(0);

        assertTrue(projections.size() ==1);
        assertTrue(projections.size() ==1 && queryProjection.getProperties().get(0).getPropertyName().equalsIgnoreCase(projectionProperty));

        assertTrue(statement.getQueryType() == AbstractQueries.QueryType.UPDATE);
        assertTrue(statement.getTarget().getTargetName().equalsIgnoreCase("contact"));

    }

    public void testDeleteQueries(){

        String propertyName = "filter_property";
        String sortingProperty = "sortingPropertyName";
        String projectionProperty = "proyectionPropertyName";
        String propertyValue = "filterPropertyValue";


        //statement
        AbstractQueries.Query statement = new AbstractQueries.DeleteQuery(new EntityId("contactId"), "contact");
        statement.withFilter(propertyName, AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue(propertyValue))
                .sortDescendingBy(sortingProperty).withProjection(new AbstractQueries.EntityProperty(projectionProperty)).build();

        //filter
        AbstractQueries.QueryFilter queryFilter = statement.getFilters().getFilterByName(propertyName);

        assertTrue(statement.getFilters().size() ==  2 /*the added plus the id restriction*/
                && queryFilter.getProperty().getPropertyName().equalsIgnoreCase(propertyName)
                && queryFilter.getValue().getValueAsString().equalsIgnoreCase(propertyValue));

        //sorting
        List<AbstractQueries.QuerySort> sorts = statement.getSorts();
        AbstractQueries.QuerySort querySort = sorts.get(0);

        assertTrue(sorts.size() ==1 &&  querySort.getProperty().getPropertyName().equalsIgnoreCase(sortingProperty)
                && querySort.getDirection() == AbstractQueries.SortDirection.DESC);

        //projection
        List<AbstractQueries.QueryProjection> projections = statement.getProjections();
        AbstractQueries.QueryProjection queryProjection = projections.get(0);

        assertTrue(projections.size() ==1);
        assertTrue(projections.size() ==1 && queryProjection.getProperties().get(0).getPropertyName().equalsIgnoreCase(projectionProperty));

        assertTrue(statement.getQueryType() == AbstractQueries.QueryType.DELETE);
        assertTrue(statement.getTarget().getTargetName().equalsIgnoreCase("contact"));

    }

    public void testCountQuery(){
        //statement
        AbstractQueries.Query statement = new AbstractQueries.Query(AbstractQueries.QueryType.SELECT, new AbstractQueries.QueryTarget("contact"));
        statement.count().build();

        assertTrue(statement.getQueryType() == AbstractQueries.QueryType.SELECT);
        assertTrue(statement.getTarget().getTargetName().equalsIgnoreCase("contact"));


    }

    public void testQueryCountMustBeASelect() {

        for (AbstractQueries.QueryType queryType : AbstractQueries.QueryType.values()) {
            if (queryType!= AbstractQueries.QueryType.SELECT) {

                boolean failed = false;

                AbstractQueries.Query statementToFail = new AbstractQueries.Query(queryType, new AbstractQueries.QueryTarget("contact"));
                try {
                    //statement
                    statementToFail.count().build();
                }
                catch (Exception e){
                    failed = true;
                }

                assertTrue(statementToFail.getTarget().getTargetName().equalsIgnoreCase("contact"));

                assertTrue(failed);
            }

        }

    }

}