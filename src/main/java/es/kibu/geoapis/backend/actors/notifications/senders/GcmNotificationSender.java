/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.senders;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.kibu.geoapis.backend.actors.messages.ApplicationRecipientData;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.results.GcmNotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodriguez2002cu on 22/04/2016.
 */
public class GcmNotificationSender implements NotificationSender {

    Logger logger = LoggerFactory.getLogger(GcmNotificationSender.class);

    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public String API_KEY = "";//"AIzaSyAAEOFf5oLpJosPRzKhrBFonIqqK6HLaf4";
    public static final String TOPICS_GLOBAL = "/topics/global";
    public static final String HTTPS_ANDROID_GOOGLEAPIS_COM_GCM_SEND = "https://android.googleapis.com/gcm/send";

    public NotificationResult send(NotificationMessage notificationMessage) {

        try {
            logger.info("about to send notification using GCM/FCM");

            // Prepare JSON containing the GCM message content. What to send and where to send.
            logger.info("sending message to target notification using GCM/FCM target: '{}', categories: {}", notificationMessage.getTo().getTarget(), notificationMessage.getTo().getCategories());

            GcmResponse responseFromTarget = null;
            if (notificationMessage.getTo().getTarget()!= null /*|| true*/) {
                responseFromTarget = sendMessage(notificationMessage, notificationMessage.getTo().getTarget());
            }
            GcmResponse responseFromCategory = null;


            if (notificationMessage.getTo() instanceof ApplicationRecipientData) {
                List<String> categories = notificationMessage.getTo().getCategories();
                logger.info("Sending messages to categories {}", categories);

                 if (categories!= null) {
                     for (String category : categories) {
                         //String category = (String) categories;
                         if (category != null) {
                             logger.info("Sending PUSH message to topic: '{}'", category);
                             responseFromCategory = sendMessage(notificationMessage, category);
                         }
                     }
                 }
            }

            //prepare the results
            GcmNotificationResult gcmNotificationResult = new GcmNotificationResult(true);

            List<GcmResponse> responses = new ArrayList<>();

            if (responseFromTarget != null) {
                responses.add(responseFromTarget);
            }

            if (responseFromCategory!= null){
                responses.add(responseFromCategory);
            }

            gcmNotificationResult.setData((Serializable) responses);

            return gcmNotificationResult;

        } catch (Exception e) {
            logger.error("Error sending:", e.getMessage(), e);
            return new GcmNotificationResult(e);
        }
    }

    public GcmResponse sendMessage(NotificationMessage notificationMessage, String target) throws IOException {
        Gson creator = new Gson();
        InputStream responseStream = sendMessageData(notificationMessage, target);
        InputStreamReader reader = new InputStreamReader(responseStream);
        //String resp = IOUtils.toString(responseStream);
        return creator.fromJson(reader/*new StringReader(resp)*/, GcmResponse.class);
    }


    public String getServiceUrl(){
        return HTTPS_ANDROID_GOOGLEAPIS_COM_GCM_SEND;
    }

    InputStream sendMessageData(NotificationMessage notificationMessage, String target) throws IOException {

        logger.debug("Preparing data to send.");

        JsonObject jGcmData = new JsonObject();
        JsonObject data = new JsonObject();
        Gson gson = new Gson();
        JsonElement message = (JsonElement) gson.toJsonTree(notificationMessage.getData());


        data.add("message", message);
        // Where to send GCM message.
        if (notificationMessage.getTo() != null) {
            //can
            jGcmData.addProperty("to", "/topics/" + target);
        }/* else {
            jGcmData.addProperty("to", TOPICS_GLOBAL);
        }*/
        // What to send in GCM message.
        jGcmData.add("data", data);

        logger.debug("Data to send: {} ", jGcmData.toString());
        // Create connection to send GCM Message request.
        URL url = new URL(getServiceUrl());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty(AUTHORIZATION, "key=" + API_KEY);
        conn.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);

        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        // Send GCM message content.
        OutputStream outputStream = conn.getOutputStream();
        logger.debug("Sending data to gcm/fcm server: {}", getServiceUrl());

        outputStream.write(jGcmData.toString().getBytes());


        // Read GCM response.
        return conn.getInputStream();
    }

    /*{ "multicast_id": 216,
            "success": 3,
            "failure": 3,
            "canonical_ids": 1,
            "results": [
        { "message_id": "1:0408" },
        { "error": "Unavailable" },
        { "error": "InvalidRegistration" },
        { "message_id": "1:1516" },
        { "message_id": "1:2342", "registration_id": "32" },
        { "error": "NotRegistered"}
        ]
    }*/

    public static class GcmResponseDetail implements Serializable{

        String message_id;
        String error;

        public GcmResponseDetail() {
            message_id = "";
            error = "";
        }

        public boolean isError(){
            return error!= null && error.equalsIgnoreCase("");
        }

        public String getValue(){
            return  isError()? error: message_id;
        }

    }

    public static class GcmResponse implements Serializable {

         int multicast_id;
         int success;
         int failure;
         int canonical_ids;

         List<GcmResponseDetail> results;

    }


    public void setAPI_KEY(String apiKey){
        this.API_KEY = apiKey;
    }

}
