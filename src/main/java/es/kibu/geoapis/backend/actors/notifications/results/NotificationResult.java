/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.results;

import es.kibu.geoapis.backend.actors.messages.ResultMessage;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 22/04/2016.
 */
public abstract class NotificationResult implements ResultMessage {

    private Serializable data;

    public void setData(Serializable data) {
        this.data = data;
    }

    public Serializable getData(){
        return data;
    }
}
