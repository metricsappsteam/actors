package es.kibu.geoapis.backend.actors.messages;

import junit.framework.TestCase;

/**
 * Created by lrodr_000 on 15/02/2017.
 */
public class IdentifiableReplyMessageTest extends TestCase {


    public void testGetRequestMessage() throws Exception {

        IdentifiableMessage identifiableMessage =  new IdentifiableMessage();
        IdentifiableReplyMessage message = new IdentifiableReplyMessage(identifiableMessage);

        assertTrue(message.getRequestMessage()!= null);
        assertTrue(identifiableMessage.equals(message.getRequestMessage()));

    }

    public void testSetRequestMessage() throws Exception {

        IdentifiableMessage identifiableMessage =  new IdentifiableMessage();
        IdentifiableMessage identifiableMessage2 =  new IdentifiableMessage();

        IdentifiableReplyMessage message = new IdentifiableReplyMessage(identifiableMessage);

        assertTrue(message.getRequestMessage().equals(identifiableMessage));
        assertTrue(message.getRequestMessage()!= null);

        message.setRequestMessage(identifiableMessage2);
        assertTrue(message.getRequestMessage().equals(identifiableMessage2));

    }

    public void testIsReplyFor() throws Exception {

    }

    public void testEquals() throws Exception {

    }

    public void testHashCode() throws Exception {

    }

}