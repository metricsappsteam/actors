/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import java.util.ArrayList;
import java.util.List;

/**
 * This interface defines the functionality for extracting the info of the queries.
 */
public interface QueryStatementTranslator {

    QueryTranslatorResult translateQuery(AbstractQueries.Query query);

    void setRootProperty(String rootProperty);

    String getRootProperty();

    AbstractQueries.Query getCurrentQuery();

    void addPropertyRenderInterceptor(PropertyRenderInterceptor propertyRenderInterceptor);

    interface PropertyRenderInterceptor {

    }

    interface PropertyTemplateRenderInterceptor extends PropertyRenderInterceptor {
        String renderPropertyTemplate(QueryStatementTranslator translator, String propertyName);
        //String renderPropertyValue(String propertyName);
    }

    interface PropertyValueRenderInterceptor extends PropertyRenderInterceptor {
        String renderPropertyValue(QueryStatementTranslator translator, String propertyName, Object propertyValue);
        //String renderPropertyValue(String propertyName);
    }


    interface QueryDef {

        void setFiltersPart(String filtersPart);

        String getFullQuery();

        String getLimitPart();

        String getFiltersPart();

        void setLimitPart(String limitPart);

        void setSkipPart(String skipPart);

        String getOrderPart();

        void setOrderPart(String orderPart);

        List<Object> getQueryParams();

        void setFullQuery(String text);

        void setQueryParams(List<Object> params);
    }

    class DefaultQueryDef implements QueryDef {

        String fullQuery;

        String filtersPart;

        String orderPart;

        String skipPart;

        String limitPart;

        public String getLimitPart() {
            return limitPart;
        }

        @Override
        public void setLimitPart(String limitPart) {
            this.limitPart = limitPart;
        }

        public String getSkipPart() {
            return skipPart;
        }

        @Override
        public void setSkipPart(String skipPart) {
            this.skipPart = skipPart;
        }

        public String getOrderPart() {
            return orderPart;
        }

        public void setOrderPart(String orderPart) {
            this.orderPart = orderPart;
        }

        private List<Object> parameters = new ArrayList<>();

        public String getFiltersPart() {
            return filtersPart;
        }

        @Override
        public void setFiltersPart(String filtersPart) {
            this.filtersPart = filtersPart;
        }

        @Override
        public String getFullQuery() {
            return fullQuery;
        }

        @Override
        public void setFullQuery(String text) {
            this.fullQuery = text;
        }

        @Override
        public void setQueryParams(List<Object> params) {
            this.parameters = params;
        }


        @Override
        public List<Object> getQueryParams() {
            return parameters;
        }

    }

    interface QueryTranslatorResult {
        QueryDef getQuery();

        int getLimit();

        int getSkip();

        boolean isCount();
    }

}
