/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.senders;

import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.recipients.Recipient;
import es.kibu.geoapis.backend.actors.notifications.results.HttpNotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 28/04/2016.
 */
@RunWith(JMockit.class)
public class GcmNotificationSenderTest {

    @Mocked
    NotificationMessage message;

    String payLoad1 = "{ \"multicast_id\": 216,\n" +
            "  \"success\": 3,\n" +
            "  \"failure\": 3,\n" +
            "  \"canonical_ids\": 1,\n" +
            "  \"results\": [\n" +
            "    { \"message_id\": \"1:0408\" },\n" +
            "    { \"error\": \"Unavailable\" },\n" +
            "    { \"error\": \"InvalidRegistration\" },\n" +
            "    { \"message_id\": \"1:1516\" },\n" +
            "    { \"message_id\": \"1:2342\", \"registration_id\": \"32\" },\n" +
            "    { \"error\": \"NotRegistered\"}\n" +
            "  ]\n" +
            "}";

    String payLoad2 = "{ \"multicast_id\": 108,\n" +
            "  \"success\": 1,\n" +
            "  \"failure\": 0,\n" +
            "  \"canonical_ids\": 0,\n" +
            "  \"results\": [\n" +
            "    { \"message_id\": \"1:08\" }\n" +
            "  ]\n" +
            "}";


    @Test
    public void testDeserializationOfDifferentPayloads() throws IOException {

        final GcmNotificationSender sender = new GcmNotificationSender();

        InputStream inputStreamPayLoad1 = IOUtils.toInputStream(payLoad1);
        InputStream inputStreamPayLoad2 = IOUtils.toInputStream(payLoad2);

        new Expectations(GcmNotificationSender.class) {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            message.getTo().getTarget();
            result = "target";

            sender.sendMessageData((NotificationMessage) any, ((String) any));
            returns(inputStreamPayLoad1, inputStreamPayLoad2);

        }};

        NotificationResult notificationResult = sender.send(message);
        assertTrue(notificationResult.isSuccess());
        assertTrue(((GcmNotificationSender.GcmResponse) ((List)notificationResult.getData()).get(0)).results.size() == 6);

        notificationResult = sender.send(message);
        assertTrue(notificationResult.isSuccess());
        assertTrue(((GcmNotificationSender.GcmResponse) ((List)notificationResult.getData()).get(0)).results.size() == 1);

        new Verifications() {{
            sender.sendMessage((NotificationMessage) any, /*(JSONCreator) any,*/ ((String) any));
            times = 2;
            sender.send((NotificationMessage) any);
            times = 2;
        }};


    }


    @Test
    public void testHttpSender() throws Exception {

        final HttpNotificationSender sender = new HttpNotificationSender();

        new Expectations(HttpNotificationSender.class) {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            sender.sendMessage((NotificationMessage) any);
            result = 200;
            message.getTo().getTarget();
            result = "http://valid.com";
        }};

        NotificationResult notificationResult = sender.send(message);
        assertTrue(notificationResult.isSuccess());
        assertTrue(((Integer) notificationResult.getData()) == 200);

        new Verifications() {{
            sender.sendMessage((NotificationMessage) any);
            times = 1;
            sender.send((NotificationMessage) any);
            times = 1;
        }};

    }

    @Test
    public void testHttpSenderFails(final @Mocked Recipient recipient) throws Exception {

        final HttpNotificationSender sender = new HttpNotificationSender();

        new Expectations(HttpNotificationSender.class) {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            sender.sendMessage((NotificationMessage) any/*, (JSONCreator) any*/);
            result = 200;
            recipient.getTarget(); returns (null, "some invalid url");
        }};

        NotificationResult notificationResult = sender.send(message);
        assertFalse(notificationResult.isSuccess());
        assertTrue(notificationResult.getData() instanceof HttpNotificationResult.HttpNotificationError);
        assertTrue(((HttpNotificationResult.HttpNotificationError)notificationResult.getData()).getError().isEmpty() == false);

        notificationResult = sender.send(message);
        assertFalse(notificationResult.isSuccess());
        assertTrue(notificationResult.getData() instanceof HttpNotificationResult.HttpNotificationError);
        assertTrue(((HttpNotificationResult.HttpNotificationError)notificationResult.getData()).getError().isEmpty() == false);


        new Verifications() {{
            sender.sendMessage((NotificationMessage) any/*, (JSONCreator) any*/);
            times = 2;
            sender.send((NotificationMessage) any);
            times = 2;
        }};

    }


    @Test
    public void testIfCommunicationFailsErrorIsExpected() throws IOException {

        final GcmNotificationSender sender = new GcmNotificationSender();
        new Expectations(GcmNotificationSender.class) {{ // an "expectation block"

            // Record an expectation, with a given value to be returned:
            message.getTo().getTarget();
            result = "target";

            sender.sendMessageData((NotificationMessage) any, /*(JSONCreator) any, */((String) any));
            result = new RuntimeException("error");
            /*returns(inputStreamPayLoad1, inputStreamPayLoad2)*/
        }};

        NotificationResult notificationResult = sender.send(message);
        assertFalse(notificationResult.isSuccess());

        new Verifications() {{
            sender.sendMessageData((NotificationMessage) any, /*(JSONCreator) any, */((String) any));
            times = 1;
        }};

    }


}