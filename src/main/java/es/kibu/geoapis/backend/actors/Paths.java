/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors;

import es.kibu.geoapis.backend.actors.notifications.NotificationActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMasterActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMongoActor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 21/12/2015.
 */
public class Paths {
    public static final String SLASH = "/";
    public static final String AKKA_DEFAULT_USER = "akka://default/user/";

    public static final String SERIALIZATION_ACTOR_PATH = AKKA_DEFAULT_USER + PersistenceMasterActor.SERIALIZATION_MASTER_ACTOR + SLASH + PersistenceMongoActor.SERIALIZATION_ACTOR;
    //public static final String FENCES_ACTOR_PATH = AKKA_DEFAULT_USER + FencesActor.FENCES_CHECK_ACTOR;
    public static final String NOTIFICATION_ACTOR_PATH = AKKA_DEFAULT_USER + NotificationActor.NOTIFICATION_ACTOR;

    public static List<String> registeredPaths = new ArrayList<>();

    static {
        registeredPaths.add(SERIALIZATION_ACTOR_PATH);
        //registeredPaths.add(FENCES_ACTOR_PATH);
        registeredPaths.add(NOTIFICATION_ACTOR_PATH);
    }

     public static boolean isValidPath(String path){
         for (String registeredPath : registeredPaths) {
             if (path.equalsIgnoreCase(registeredPath)) {
                 return true;
             }
         }
         return false;
     }

}
