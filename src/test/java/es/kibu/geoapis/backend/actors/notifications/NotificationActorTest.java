/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import es.kibu.geoapis.backend.actors.MainActorSystem;
import es.kibu.geoapis.backend.actors.Paths;
import es.kibu.geoapis.backend.actors.notifications.configurators.ConfiguratorFactory;
import es.kibu.geoapis.backend.actors.notifications.configurators.SenderConfigurator;
import es.kibu.geoapis.backend.actors.notifications.recipients.Recipient;
import es.kibu.geoapis.backend.actors.notifications.results.BaseNotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.SimpleNotificationResult;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;
import es.kibu.geoapis.backend.actors.notifications.senders.SenderFactory;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by lrodriguez2002cu on 26/04/2016.
 */

@RunWith(JMockit.class)
public class NotificationActorTest {

    private static ActorSystem system;
    Logger logger = LoggerFactory.getLogger(NotificationActorTest.class);

    @BeforeClass
    public static void setUp() throws Exception {
        //GeoFencesServer.startSystem();
        system = MainActorSystem.getActorSystem();
        //system = ActorSystem.create();
    }

    @Injectable
    Recipient recipient;

    @Mocked
    NotificationMessageInfo notificationMessageInfo;

    @Mocked
    SenderConfigurator senderConfigurator;

    private UserInfo createSampleUserInfo() {
        UserInfo userInfo = new UserInfo();
        return userInfo;
    }

    @Test
    public void testAndCheckSenderIsCalled(@Mocked ConfiguratorFactory factory, @Capturing
            NotificationSender sender/*, @Mocked SenderFactory senderFactory*/) {

        final NotificationResult notificationResult = new BaseNotificationResult(true);

        new Expectations() {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:\
            //senderFactory.createSender((NotificationMessage) any); result = sender;
            /*recipient.getPlatform();
            result = "android";*/
            sender.send((NotificationMessage) any);
            result = notificationResult;
            ConfiguratorFactory.createConfigurator((NotificationMessage) any, (ActorRef) any);
            result = senderConfigurator;
            senderConfigurator.configure((NotificationSender) any, (NotificationMessage) any);
        }};

        NotificationMessage msg = NotificationMessageFactory.createMessage(NotificationMessage.NotificationType.PUSH, recipient, notificationMessageInfo);

        new JavaTestKit(system) {{
            final Props props = NotificationActor.props();
            final ActorRef subject = system.actorOf(props);

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds
            new Within(duration("1 second")) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    expectNoMsg();
                }
            };
        }};

        new Verifications() {{
            //a sender send fucntion was invoked one time
            sender.send((NotificationMessage) any);
            times = 1;
            //the configurator was called
            senderConfigurator.configure((NotificationSender) any, (NotificationMessage) any);
        }};
    }

    @Test
    public void testItWithActorSenderTheTargetActorGetsAMessage() {
        /*
         * Wrap the whole test procedure within a testkit constructor
         * if you want to receive actor replies or use Within(), etc.
         */
        // can also use JavaTestKit “from the outside”
        final JavaTestKit probe = new JavaTestKit(system);
        final String actorsTargetPath = probe.getRef().path().toString();

        new Expectations() {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            recipient.getTarget();
            result = actorsTargetPath; /*name()*/
        }};

        new Expectations(Paths.class) {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            Paths.isValidPath(anyString);
            result = true;
        }};

        NotificationMessage msg = NotificationMessageFactory.createMessage(NotificationMessage.NotificationType.ACTOR, recipient, notificationMessageInfo);

        new JavaTestKit(system) {{
            final Props props = NotificationActor.props();
            final ActorRef subject = system.actorOf(props);

            new Within(new FiniteDuration(3, TimeUnit.SECONDS)/*duration("500 seconds")*/) {
                protected void run() {
                    subject.tell(msg, getRef());
                    probe.expectMsgClass(NotificationMessage.class);
                    expectNoMsg();
                }
            };
        }};

    }


    @Test
    public void testItWithActorSenderTheTargetActorDoesNotGetAMessage(
            @Mocked NotificationActor.ResultInterceptor interceptor /*,@Mocked SimpleNotificationResult notificationResult*/) {
    /*
     * Wrap the whole test procedure within a testkit constructor
     * if you want to receive actor replies or use Within(), etc.
     */

        final String path = "some invalid path";
        new Expectations() {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            recipient.getTarget();
            result = path;
            times = 1;
        }};

        new Expectations(Paths.class) {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            Paths.isValidPath(anyString);
            result = true;

        }};

        // can also use JavaTestKit “from the outside”
        //final JavaTestKit probe = new JavaTestKit(system);

        NotificationMessage msg = NotificationMessageFactory.
                createMessage(NotificationMessage.NotificationType.ACTOR, recipient, notificationMessageInfo);

        new JavaTestKit(system) {{
            final Props props = NotificationActor.props();
            final ActorRef subject = system.actorOf(props);

            new Within(new FiniteDuration(5, TimeUnit.SECONDS)/*duration("500 seconds")*/) {
                protected void run() {
                    subject.tell(msg, getRef());
                    //probe.expectMsgClass(NotificationMessage.class);
                    expectNoMsg();
                }
            };
        }};

        new Verifications() {{

            //interceptor.notifyResult((SimpleNotificationResult)any); times = 1;
            NotificationResult notificationResult1;
            interceptor.notifyResult(notificationResult1 = withCapture());
            times = 1;

            //new SimpleNotificationResult(withEqual(false)); times = 1;
            new SimpleNotificationResult(false);
            times = 1;
            assertTrue(notificationResult1 instanceof SimpleNotificationResult);
            //logger.debug("Was success:" + Boolean.toString(result.isSuccess()));
            //assertFalse(notificationResult1.isSuccess());
        }};

    }

    @AfterClass
    public static void tearDown() throws Exception {
        if (system != null) {
            MainActorSystem.terminateAndWait();
        }
    }

}