package es.kibu.geoapis.backend.actors.tools;

import akka.actor.Cancellable;
import akka.actor.Props;
import akka.actor.UntypedActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 03/03/2017.
 */
public abstract class ToolsActor extends UntypedActor {

    Logger logger = LoggerFactory.getLogger(ToolsActor.class);
    Cancellable tick;

    public class HeartbeatMessage implements Serializable{

    }

    public abstract boolean getDoHeartbeat();

    public static Props props(){
        return Props.create(ToolsActor.class);
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        logger.debug("ToolsActor is running...");
        if (message instanceof HeartbeatMessage){
                logger.debug("Hearbeat happened");
        }
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();

        if (getDoHeartbeat()) {
            tick = getContext().system().scheduler().schedule(new FiniteDuration(500, TimeUnit.MILLISECONDS), getInterval(),
                    getSelf(), new HeartbeatMessage(), getContext().system().dispatcher(), getSelf());
        }

    }

    protected FiniteDuration getInterval() {
        return new FiniteDuration(1000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        if (tick!= null) {
            tick.cancel();
        }
    }

}
