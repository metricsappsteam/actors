package es.kibu.geoapis.backend.actors.notifications.configurators;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by lrodriguez2002cu on 19/01/2017.
 * This is a configurator that is able to ask another actor for the configuration of a given sender.
 */
public abstract class ActorDeferredConfigurator implements SenderConfigurator {

    Logger logger = LoggerFactory.getLogger(ActorDeferredConfigurator.class);

    public abstract ActorRef getActorConfiguratorRef();


    public static class ConfigurationResponseMessage implements Serializable {

        Map<String, Serializable> keyValues = new HashMap<>();

        public void addConfigKeyValue(String key, Serializable value) {
            keyValues.put(key, value);
        }

        public Serializable getConfig(String key) {
            return keyValues.get(key);
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        boolean error;
        String errorMessage;

        public static ConfigurationResponseMessage forError(String errorMessage) {
            ConfigurationResponseMessage responseMessage = new ConfigurationResponseMessage();
            responseMessage.errorMessage = errorMessage;
            responseMessage.error = true;
            return responseMessage;
        }



    }

    public static class ConfigurationRequestMessage implements Serializable {

        NotificationMessage message;

        public NotificationMessage getMessage() {
            return message;
        }

        public void setMessage(NotificationMessage message) {
            this.message = message;
        }

        public NotificationMessage.NotificationType getType() {
            return message.getType();
        }

        public ConfigurationRequestMessage(NotificationMessage message) {
            this.message = message;
        }


    }

    ConfigurationRequestMessage getConfigurationMessage(NotificationMessage message) {
        return new ConfigurationRequestMessage(message);
    }

    @Override
    public void configure(NotificationSender sender, NotificationMessage message) {

        ConfigurationRequestMessage configurationMessage = getConfigurationMessage(message);

        try {
            Timeout timeout = new Timeout(10, TimeUnit.SECONDS);
            Future<Object> future = Patterns.ask(getActorConfiguratorRef(), configurationMessage, timeout);
            ConfigurationResponseMessage result = (ConfigurationResponseMessage) Await.result(future, timeout.duration());
            performConfiguration(result, sender, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract void performConfiguration(ConfigurationResponseMessage result, NotificationSender sender, NotificationMessage message);
}
