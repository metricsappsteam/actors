/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import es.kibu.geoapis.backend.actors.messages.EntityId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a small DSL for expressing "simple" queries in a query language independent way.
 * Created by lrodriguez2002cu on 14/12/2015.
 */

public class AbstractQueries {

    public static class QueryOperator<T> implements Serializable {

        private String operatorName;

        private T operator;

        public T getOperator() {
            return operator;
        }

        public String getName() {

            if (operatorName != null) {
                return operatorName;
            }

            if (operator instanceof Enum) {
                return ((Enum) operator).name();
            }

            if (operator instanceof NamedObject) {
                return ((NamedObject) operator).getName();
            }

            throw new RuntimeException("The query operator must have a name");
        }

        public void setOperator(T operator) {
            this.operator = operator;
        }

        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        @Override
        public String toString() {
            return "QueryOperator{" +
                    "operatorName='" + operatorName + '\'' +
                    ", operator=" + operator +
                    '}';
        }
    }

    interface NamedObject {
        String getName();
    }


    /*
    $eq	Matches values that are equal to a specified value.
    $gt	Matches values that are greater than a specified value.
    $gte	Matches values that are greater than or equal to a specified value.
    $lt	Matches values that are less than a specified value.
    $lte	Matches values that are less than or equal to a specified value.
    $ne	Matches all values that are not equal to a specified value.
    $in	Matches any of the values specified in an array.
    $nin	Matches none of the values specified in an array.

    */
    public enum ComparisonOp implements NamedObject {
        equal, lt, gt, notequal, gte, lte, in, nin, regex, range, withId, elemMatch;

        @Override
        public String getName() {
            return name();
        }

        @Override
        public String toString() {
            return "ComparisonOp{ name = }";
        }
    }


    public static class ComparisonOperators extends QueryOperator<ComparisonOp> {

        ComparisonOperators(ComparisonOp op) {
            this.setOperator(op);
        }

        public static ComparisonOperators EQUAL = new ComparisonOperators(ComparisonOp.equal);
        public static ComparisonOperators LT = new ComparisonOperators(ComparisonOp.lt);
        public static ComparisonOperators GT = new ComparisonOperators(ComparisonOp.gt);
        public static ComparisonOperators NOT_EQUAL = new ComparisonOperators(ComparisonOp.notequal);

        public static ComparisonOperators IN = new ComparisonOperators(ComparisonOp.in);
        public static ComparisonOperators GTE = new ComparisonOperators(ComparisonOp.gte);
        public static ComparisonOperators LTE = new ComparisonOperators(ComparisonOp.lte);
        public static ComparisonOperators NIN = new ComparisonOperators(ComparisonOp.nin);

        public static ComparisonOperators WITH_ID = new ComparisonOperators(ComparisonOp.withId);
        public static ComparisonOperators ELEM_MATCH = new ElemFilterOperator();

    }

    public static class REG_EX extends ComparisonOperators {

        public String getPattern() {
            return pattern;
        }

        public void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public String getOptions() {
            return options;
        }

        public void setOptions(String options) {
            this.options = options;
        }

        String pattern;
        String options;

        public REG_EX(String pattern, String options) {
            super(ComparisonOp.regex);
            this.pattern = pattern;
            this.options = options;
        }
    }

    /** This Operator is intended to be used with documents where the match have to occur against properties of an array
     */
    //{ "entity.points": { $elemMatch: { uuid: "uuid765"} } }
    public static class ElemFilterOperator extends ComparisonOperators {

        public ElemFilterOperator() {
            super(ComparisonOp.elemMatch);
        }
    }

    public static class RangeOperator extends ComparisonOperators {
        public RangeOperator() {
            super(ComparisonOp.range);
        }
    }

    public static class SpatialOperators extends QueryOperator<SpatialOp> {

        SpatialOperators(SpatialOp op) {
            this.setOperator(op);
        }

        public static SpatialOperators WITHIN = new SpatialOperators(SpatialOp.within);

    }


    public static class EntityProperty implements Serializable {

        String propertyName;

        public EntityProperty(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

    }

    public static class QueryValue<T extends Serializable> implements Serializable {
        private T value;

        public QueryValue(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public String getValueAsString() {
            return value.toString();
        }

        public Double getValueAsDouble() {
            return (Double) value;
        }

        public Integer getValueAsInteger() {
            return (Integer) value;
        }

        @Override
        public String toString() {
            return "QueryValue{" +
                    "value=" + value +
                    '}';
        }
    }

    public static class QueryPart implements Serializable {

    }


    /**
     * The target is intended to provide the target(i.e collection on MondoDB)
     */
    public static class QueryTarget extends QueryPart {

        private String target;

        public String getTargetName() {
            return target;
        }

        public void setTargetName(String target) {
            this.target = target;
        }

        public QueryTarget(String target) {
            this.target = target;
        }

        @Override
        public String toString() {
            return "QueryTarget{" +
                    "target='" + target + '\'' +
                    '}';
        }
    }

    public static class QueryFilter extends QueryPart {

        private EntityProperty property;
        private QueryOperator operator;
        private QueryValue value;

        public EntityProperty getProperty() {
            return property;
        }

        public QueryOperator getOperator() {
            return operator;
        }

        public QueryValue getValue() {
            return value;
        }

        public QueryFilter(EntityProperty property, QueryOperator operator, QueryValue value) {
            this.property = property;
            this.operator = operator;
            this.value = value;
        }

        public QueryFilter(String propertyName, QueryOperator operator, QueryValue value) {
            this.property = new EntityProperty(propertyName);
            this.operator = operator;
            this.value = value;
        }

        public QueryFilter(String propertyName, QueryOperator operator, Serializable value) {
            this.property = new EntityProperty(propertyName);
            this.operator = operator;
            this.value = (value instanceof QueryValue) ? (QueryValue) value : new QueryValue(value);
        }

        @Override
        public String toString() {
            return "QueryFilter{" +
                    "property=" + property +
                    ", operator=" + operator +
                    ", value=" + value +
                    '}';
        }
    }

    public static class QueryProjection extends QueryPart {

        List<EntityProperty> properties = new ArrayList<EntityProperty>();

        public List<EntityProperty> getProperties() {
            return properties;
        }

        public QueryProjection(List<EntityProperty> property) {
            this.properties = property;
        }

        public QueryProjection(EntityProperty property) {
            this.properties.add(property);
        }

        public void addProperty(EntityProperty prop) {
            properties.add(prop);
        }

        public EntityProperty getProperty(String propertyName) {
            for (EntityProperty property : properties) {
                if (property.getPropertyName().equalsIgnoreCase(propertyName)) {
                    return property;
                }
            }
            return null;
        }

        public EntityProperty getProperty(int index) {
            return properties.get(index);
        }

        @Override
        public String toString() {
            return "QueryProjection{" +
                    "properties=" + properties +
                    '}';
        }
    }

    public enum SortDirection {ASC, DESC}


    public static class QuerySort extends QueryPart {

        private EntityProperty property;

        public SortDirection getDirection() {
            return direction;
        }

        public void setDirection(SortDirection direction) {
            this.direction = direction;
        }

        public EntityProperty getProperty() {
            return property;
        }

        public void setProperty(EntityProperty property) {
            this.property = property;
        }

        private SortDirection direction;

        public QuerySort(EntityProperty property, SortDirection direction) {
            this.property = property;
            this.direction = direction;
        }

        @Override
        public String toString() {
            return "QuerySort{" +
                    "property=" + property +
                    ", direction=" + direction +
                    '}';
        }
    }

    /**
     * The query type..
     */

    public enum QueryType {
        INSERT, DELETE, SELECT, UPDATE
    }

    public static class InsertQuery extends Query {
        public Object getEntity() {
            return entity;
        }

        public void setEntity(Object entity) {
            this.entity = entity;
        }

        //the entity (if needed)
        private Object entity;

        public InsertQuery(Object entity, String target) {
            super(QueryType.INSERT, new QueryTarget(target));
            this.entity = entity;
        }

        @Override
        public String toString() {
            return "InsertQuery{" +
                    "entity=" + entity +
                    '}';
        }
    }


    public static class SelectQuery extends Query {

        //the class expected as a result
        private Class resultClass;


        public Class getResultClass() {
            return resultClass;
        }

        public void setResultClass(Class resultClass) {
            this.resultClass = resultClass;
        }

        public SelectQuery(Class resultClass, String target) {
            super(QueryType.SELECT, new QueryTarget(target));
            this.resultClass = resultClass;
        }

        @Override
        public String toString() {
            return "SelectQuery{" +
                    "resultClass=" + resultClass +
                    '}';
        }
    }

    public static class UpdateQuery extends Query {

        public Object getEntity() {
            return entity;
        }

        public void setEntity(Object entity) {
            this.entity = entity;
        }

        //the entity (if needed)
        private Object entity;

        public UpdateQuery(Object entity, String target) {
            super(QueryType.UPDATE, new QueryTarget(target));
            this.entity = entity;

        }

        @Override
        public String toString() {
            return "UpdateQuery{" +
                    "entity=" + entity +
                    '}';
        }
    }


    public static class DeleteQuery extends Query {

        public EntityId  getEntityId() {
            return entityId;
        }

        public void setEntityId(EntityId  entityId) {
            this.entityId = entityId;
        }

        //the entity (if needed)
        private EntityId entityId;

        public DeleteQuery(EntityId entityId, String target) {
            super(QueryType.DELETE, new QueryTarget(target));
            this.entityId = entityId;
            if (entityId != null) PersistenceMessageUtils.queryWithId(this, entityId);
        }

        @Override
        public String toString() {
            return "DeleteQuery{" +
                    "entityId=" + entityId +
                    '}';
        }
    }


    public static class Query extends QueryStatement {
        //the type of query
        public Query(QueryType type, QueryTarget target) {
            this.setQueryType(type);
            this.setTarget(target);
        }
    }


    public static class Filters {

        List<QueryFilter> filters = new ArrayList<>();

        public void add(QueryFilter filter) {
            filters.add(filter);
        }

        public List<AbstractQueries.QueryFilter> getFilters() {
            return filters;
        }

        public QueryFilter getFilterByName(String propertyName) {

            for (QueryFilter filter : filters) {
                if (filter.getProperty().getPropertyName().equalsIgnoreCase(propertyName)) {
                    return filter;
                }
            }
            return null;
        }

        public int size() {
            return filters.size();
        }

        public QueryFilter get(int i) {
            return filters.get(i);
        }

        @Override
        public String toString() {
            return "Filters{" +
                    "filters=" + filters +
                    '}';
        }
    }

    public abstract static class QueryStatement implements Serializable, QueryFilterable, QuerySortable, QueryProjectable {

        Logger logger = LoggerFactory.getLogger(QueryStatement.class);
        private QueryType type;

        private Filters filters = new Filters();

        private List<QuerySort> sorts = new ArrayList<QuerySort>();

        private List<QueryProjection> projections = new ArrayList<QueryProjection>();

        private int limit = 0;

        private boolean isCountQuery;

        private int skip;

        public void setTarget(QueryTarget target) {
            this.target = target;
        }

        private QueryTarget target;

        public QueryType getQueryType() {
            return type;
        }

        public void setQueryType(QueryType type) {
            this.type = type;
        }

        public QueryTarget getTarget() {
            return target;
        }


        public int getLimit() {
            return limit;
        }

        public boolean isCountQuery() {
            return isCountQuery;
        }

        public int getSkip() {
            return skip;
        }

        public Filters getFilters() {
            return filters;
        }

        public List<QuerySort> getSorts() {
            return sorts;
        }

        public List<QueryProjection> getProjections() {
            return projections;
        }

        //todo: maybe add aggregations
        @Override
        public QueryFilterable withFilter(EntityProperty property, QueryOperator operator, QueryValue value) {
            filters.add(new QueryFilter(property, operator, value));
            return this;
        }

        @Override
        public QueryFilterable withFilter(String propertyName, QueryOperator operator, QueryValue value) {
            filters.add(new QueryFilter(new EntityProperty(propertyName), operator, value));
            return this;
        }

        @Override
        public QueryFilterable withFilter(String propertyName, QueryOperator operator, String value) {
            filters.add(new QueryFilter(new EntityProperty(propertyName), operator, new QueryValue(value)));
            return this;
        }

        @Override
        public QueryFilterable withFilter(QueryFilter filter) {
             if (filter!= null) {
                 filters.add(filter);
             }
            else {
                 logger.warn("A filter with null value, was set to query..");
             }
            return this;
        }


        @Override
        public QueryFilterable limit(int limit) {
            this.limit = limit;
            return this;
        }


        public QueryFilterable skip(int skip) {
            this.skip = skip;
            return this;
        }


        @Override
        public QuerySortable sortBy(EntityProperty property, SortDirection direction) {
            QuerySort sortBy = new QuerySort(property, direction);
            sorts.add(sortBy);
            return this;
        }

        @Override
        public QuerySortable sortDescendingBy(EntityProperty property) {
            return sortBy(property, SortDirection.DESC);
        }

        @Override
        public QuerySortable sortDescendingBy(String propertyName) {
            return sortBy(new EntityProperty(propertyName), SortDirection.DESC);
        }

        @Override
        public QuerySortable sortBy(String propertyName, SortDirection direction) {
            return sortBy(new EntityProperty(propertyName), direction);
        }


        public QueryProjectable withProjection(EntityProperty property) {
            projections.add(new QueryProjection(property));
            return this;
        }

        //todo: maybe makes sense to leave this method to be abstract so that the actual
        //encoder to the
        public void build() {

        }

        @Override
        public QueryCreator count() {
            if (this.getQueryType() != null && this.getQueryType() != QueryType.SELECT) {
                throw new InvalidQueryOperationException("The count operator can used ONLY with SELECT queries (QueryType.SELECT)");
            }
            isCountQuery = true;
            return this;
        }

        @Override
        public String toString() {
            return "QueryStatement{" +
                    "type=" + type +
                    ", filters=" + filters +
                    ", sorts=" + sorts +
                    ", projections=" + projections +
                    ", limit=" + limit +
                    ", isCountQuery=" + isCountQuery +
                    ", skip=" + skip +
                    ", target=" + target +
                    '}';
        }
    }

    public static class InvalidQueryOperationException extends RuntimeException {
        public InvalidQueryOperationException(String s) {
            super(s);
        }

        public InvalidQueryOperationException(String s, Throwable throwable) {
            super(s, throwable);
        }

        public InvalidQueryOperationException(Throwable throwable) {
            super(throwable);
        }

        public InvalidQueryOperationException(String s, Throwable throwable, boolean b, boolean b1) {
            super(s, throwable, b, b1);
        }
    }

    /**
     * Created by lrodr_000 on 14/12/2015.
     */
    public interface QueryCreator {
        void build();
    }

    /**
     * Created by lrodr_000 on 14/12/2015.
     */
    public interface QueryFilterable extends QueryProjectable, QuerySortable {

        QueryFilterable withFilter(EntityProperty property, QueryOperator operator, QueryValue value);

        QueryFilterable withFilter(String propertyName, QueryOperator operator, QueryValue value);

        QueryFilterable withFilter(String propertyName, QueryOperator operator, String value);

        QueryFilterable withFilter(QueryFilter filter);

        QueryFilterable limit(int limit);
    }

    public interface QueryCountable extends QueryCreator {

        QueryCreator count();

    }


    /**
     * Created by lrodr_000 on 14/12/2015.
     */
    public interface QueryProjectable extends QueryCountable {
        QueryProjectable withProjection(EntityProperty property);
    }

    /**
     * Created by lrodr_000 on 14/12/2015.
     */
    public interface QuerySortable extends QueryProjectable {

        QuerySortable sortBy(EntityProperty property, SortDirection direction);

        QuerySortable sortDescendingBy(EntityProperty property);

        QuerySortable sortDescendingBy(String propertyName);

        QuerySortable sortBy(String propertyName, SortDirection direction);
    }

    /**
     * Created by lrodr_000 on 14/12/2015.
     */
    public enum SpatialOp {
        within
    }
}
