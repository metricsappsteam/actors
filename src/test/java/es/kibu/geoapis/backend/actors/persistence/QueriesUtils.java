/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import es.kibu.geoapis.backend.actors.messages.ApplicationId;
import es.kibu.geoapis.backend.actors.messages.UserId;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 14/05/2016.
 */
public class QueriesUtils {
/*

    public static AbstractQueries.QueryStatement getQuerySelectStatementRangeFilterAndIdFilter(String appId, DateTime minDate,
                                                                                               DateTime maxDate,
                                                                                               String target, String dateProperty,
                                                                                               Class<TriggerCreateParams> resultClass ) {

        //Class<TriggerCreateParams> resultClass = TriggerCreateParams.class;
        AbstractQueries.QueryStatement statement = new AbstractQueries.SelectQuery(resultClass, target);

        List<DateTime> ranges = new ArrayList<>();
        ranges.add(minDate);
        ranges.add(maxDate);
        //String dateProperty = "date_to";
        statement.withFilter(dateProperty, new AbstractQueries.RangeOperator(), new AbstractQueries.QueryValue((Serializable) ranges));
        statement.sortBy(dateProperty, AbstractQueries.SortDirection.DESC);
        AbstractQueries.QueryFilter appFilter = PersistenceMessageUtils.applicationIdFilter(new ApplicationId(appId));
        statement.withFilter(appFilter);

        return statement;
    }


    public static AbstractQueries.QueryStatement filterTriggersByDates(ApplicationId applicationId, DateTime minDate,
                                                                 DateTime maxDate) {
        String target = "trigger";
        String dateProperty = "date_to";
        AbstractQueries.QueryStatement query = getQuerySelectStatementRangeFilterAndIdFilter(applicationId.getId(),
                minDate, maxDate, target, dateProperty, TriggerCreateParams.class);

        return query;
    }
*/

    public static AbstractQueries.QueryStatement  getSelectQueryWithLimit(ApplicationId  applicationId, String target,
                                                                   Class<?> resultClass, int limit){

        AbstractQueries.QueryStatement query = new AbstractQueries.SelectQuery(resultClass, target);
        query.limit(limit);
        query.withFilter(PersistenceMessageUtils.applicationIdFilter(applicationId));

        return query;
    }

    /*public static AbstractQueries.QueryStatement filterLocationsWithLimit(ApplicationId applicationId, UserId userId, int limit) {

        String target = "location";
        AbstractQueries.QueryStatement query = getSelectQueryWithLimit(applicationId,
                 target, LocationUpdateParams.class, limit);
        AbstractQueries.QueryFilter queryFilter = PersistenceMessageUtils.userIdFilter(userId);
        query.withFilter(queryFilter);
        return query;
    }*/

}
