/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.configurators;

import es.kibu.geoapis.backend.actors.messages.ApplicationRecipientData;
import es.kibu.geoapis.backend.actors.messages.ApplicationId;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.senders.GcmNotificationSender;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;
/*
import es.kibu.geoapis.geofences.backend.security.managers.ApplicationsManager;
import es.kibu.geoapis.geofences.backend.security.managers.DefaultApplicationsManager;
import es.kibu.geoapis.geofences.client.applications.ApplicationsCreateParams;
*/

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 25/04/2016.
 */
public class GcmConfigurator implements SenderConfigurator {

    //ApplicationsManager applicationsManager = DefaultApplicationsManager.getInstance();

    Map<String, String> applicationsAndKeys = new HashMap<>();

    public String getApiKey(String applicationId){
        if (applicationsAndKeys.containsKey(applicationId)) {
            return applicationsAndKeys.get(applicationId);
        }
        return null;
    }


    public void configure(NotificationSender sender, NotificationMessage message) {

         if (sender instanceof GcmNotificationSender) {
             if ((message.getTo() instanceof ApplicationRecipientData)) {
                 //String userId = ((ApplicationRecipientData) message.getTo()).getUser();
                 String applicationIdStr = ((ApplicationRecipientData) message.getTo()).getApplication();
                 //UsersManagerFactory.getUsersManager().getUserById(userId, )
                 ApplicationId applicationId = new ApplicationId(applicationIdStr);

                 if (applicationsExists(applicationId)) {
                     obtainApiKeyIfNeeded(applicationId);
                     String apiKey = getApiKey(applicationIdStr);
                     if (apiKey!= null && !apiKey.isEmpty()){
                         ((GcmNotificationSender)sender).setAPI_KEY(apiKey);
                     }
                 }
             }
         }
    }

    protected boolean applicationsExists(ApplicationId id) {
        //applicationsManager.applicationsExists(applicationId)
        return true;
    }

    public void obtainApiKeyIfNeeded(ApplicationId applicationId) {

        throw  new RuntimeException("Feature not implemented");
        /*if (!applicationsAndKeys.containsKey(applicationId.getId())) {
            ApplicationsCreateParams applicationsCreateParams = applicationsManager.applicationById(applicationId);
            registerApiKey(applicationId, applicationsCreateParams.gcm_api_key);
        }*/
    }

    private void registerApiKey(ApplicationId applicationId, String gcm_api_key) {
        applicationsAndKeys.put(applicationId.getId(), gcm_api_key);
    }
}
