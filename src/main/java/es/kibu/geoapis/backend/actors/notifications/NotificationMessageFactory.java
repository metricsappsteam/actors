/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;

import es.kibu.geoapis.backend.actors.notifications.recipients.Recipient;

/**
 * Created by lrodriguez2002cu on 25/04/2016.
 */
public class NotificationMessageFactory {

    public static class InvalidNotificationMessageCreationParams extends RuntimeException {
        public InvalidNotificationMessageCreationParams(String s) {
            super(s);
        }
    }

    @Deprecated
    public static NotificationMessage createMessage(NotificationMessage.NotificationType type, Recipient recipient,
                                                    NotificationMessageInfo messageInfo) {
        if (type == null || recipient == null || messageInfo == null){
            throw new InvalidNotificationMessageCreationParams("None of the parameters type, recipient , messageInfo can be null");
        }

        return new NotificationMessage() {
            @Override
            public NotificationType getType() {
                return type;
            }

            @Override
            public Recipient getTo() {
                return recipient;
            }

            @Override
            public String getData() {
                return messageInfo.toString();
            }
        };
    }

    public static NotificationMessage createMessage(NotificationMessage.NotificationType type, Recipient recipient,
                                                    String payload) {
        if (type == null || recipient == null || payload == null){
            throw new InvalidNotificationMessageCreationParams("None of the parameters type, recipient , messageInfo can be null");
        }

        return new NotificationMessage() {
            @Override
            public NotificationType getType() {
                return type;
            }

            @Override
            public Recipient getTo() {
                return recipient;
            }

            @Override
            public String getData() {
                return payload;
            }
        };
    }

    @Deprecated
    public static NotificationMessage createPUSHMessage(Recipient recipient, NotificationMessageInfo messageInfo) {
        return createMessage(NotificationMessage.NotificationType.PUSH, recipient, messageInfo);
    }


    public static NotificationMessage createPUSHMessage(Recipient recipient, String payload) {
        return createMessage(NotificationMessage.NotificationType.PUSH, recipient, payload);
    }


}
