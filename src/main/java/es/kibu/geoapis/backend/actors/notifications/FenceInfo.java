/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;

import es.kibu.geoapis.backend.actors.GeoUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 22/04/2016.
 */
public class FenceInfo<T extends Serializable> {

    //todo: might need to be renamed in the future
    T fence;
    String description;
    Map<String, String> extras;
    String name;

    public Map<String, String> getExtras() {
        return extras;
    }

    public void setExtras(Map<String, String> extras) {
        this.extras = extras;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getFence() {
        return fence;
    }

    public void setFence(T fence) {
        this.fence = fence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FenceInfo(T layer, String description, Map<String, String> extras, String name) {
        this.fence = layer;
        this.description = description;
        this.extras = extras;
        this.name = name;
    }
}
