/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import es.kibu.geoapis.backend.actors.notifications.NotificationActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMongoActor;

/**
 * Created by lrodriguez2002cu on 30/03/2016.
 */
public class ActorsUtils {

    public static final String serializationRouterName = "serializationRouter";

    public static final String fencesRouterName = "fencesRouter";

    public static final String notificationRouterName = "notificationRouter";

    static ActorRef persistenceRouter;

    static ActorRef notificationRouter;

    static ActorRef fencesRouter;

    public static class InvalidActorPath extends RuntimeException {

        public InvalidActorPath(String s) {
            super(s);
        }
    }

    public static ActorSelection getPersistenceActorSelection() {
        ActorSystem system = MainActorSystem.getActorSystem();
        ActorSelection serializationActorSelection = system.actorSelection(Paths.SERIALIZATION_ACTOR_PATH);
        return serializationActorSelection;
    }

    /*public static ActorSelection getFencesActorSelection() {
        ActorSystem system = MainActorSystem.getActorSystem();
        ActorSelection fencesActorSelection = system.actorSelection(Paths.FENCES_ACTOR_PATH);
        return fencesActorSelection;
    }
*/
    public static ActorSelection getNotificationActorSelection() {
        ActorSystem system = MainActorSystem.getActorSystem();
        ActorSelection notificationActorSelection = system.actorSelection(Paths.NOTIFICATION_ACTOR_PATH);

        return notificationActorSelection;
    }

    public static ActorSelection forPath(String path) {
        if (Paths.isValidPath(path)) {
            ActorSystem system = MainActorSystem.getActorSystem();
            ActorSelection actorSelection = system.actorSelection(path);
            return actorSelection;
        } else throw new InvalidActorPath(String.format("Invalid Actor path(%s)", path));
    }

    @Deprecated
    public static ActorRef getPersistenceActor() {
        return getPersistenceActor(null);
    }

    public static ActorRef getPersistenceActor(ActorSystem actorSystem) {

        if (persistenceRouter == null || persistenceRouter.isTerminated()) {
            persistenceRouter = createRouter(serializationRouterName, PersistenceMongoActor.class, actorSystem);
        }
        return persistenceRouter;
    }

    @Deprecated
    public static ActorRef getNotificationActor() {
        return getNotificationActor(null);
    }

    public static ActorRef getNotificationActor(ActorSystem actorSystem){
        if (notificationRouter == null || notificationRouter.isTerminated()) {
            notificationRouter = createRouter(notificationRouterName, NotificationActor.class, actorSystem);
        }
        return notificationRouter;
    }

    /*public static ActorRef getFencesActor() {
        if (fencesRouter == null || fencesRouter.isTerminated()) {
            fencesRouter = createRouter(fencesRouterName, FencesActor.class);
        }
        return fencesRouter;
    }*/

    private static ActorRef createRouter(String name, Class childActorClass, ActorSystem actorSystem) {
        ActorSystem system = (actorSystem==null)? MainActorSystem.getActorSystem() : actorSystem;
        if (system!= null || !system.isTerminated()) {
            ActorRef router = system.actorOf(FromConfig.getInstance().props(Props.create(childActorClass)),
                       /*"router"*//*SeqActorName.apply(name.toLowerCase()).next()*/name );
            return router;
        }
        throw new RuntimeException("The actor system is down!!.  Impossible to create routers!!");
    }

    /*
    private static ActorRef createRouter(String name, Class childActorClass) {
        ActorSystem system = MainActorSystem.getActorSystem();
        if (system != null || !system.isTerminated()) {
            ActorRef router = system.actorOf(FromConfig.getInstance().props(Props.create(childActorClass)),
                       name);
            return router;
        }
        throw new RuntimeException("The actor system is down!!.  Impossible to create routers!!");
    }
    */


}
