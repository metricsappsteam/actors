/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.senders;

import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.configurators.SenderConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodr_000 on 25/04/2016.
 */
public class SenderFactory {

    static Logger logger = LoggerFactory.getLogger(SenderFactory.class);

    public NotificationSender createSender(NotificationMessage message){
        return createSender(message, null);
    }

     public static NotificationSender createSender(NotificationMessage message, SenderConfigurator configurator){

        NotificationSender sender = null;
        switch (message.getType()) {
            case SMS:
                sender = new SMSNotificationSender(/*message*/);
                break;
            case PUSH:
                sender = getPushNotificationSender(message);
                break;
            case CALL_URL:
                sender = new HttpNotificationSender(/*message*/);
                break;
            case ACTOR:
                sender = new ActorNotificationSender(/*message*/);
                break;
        }

        if (sender==null) {
            throw new RuntimeException(String.format("No sender was created for the message type %s", message.getType()));
        }

        if (configurator!= null){
            configurator.configure(sender, message);
        }
       return sender;
    }

    private static NotificationSender getPushNotificationSender(NotificationMessage message) {
        NotificationSender sender = null;
        logger.info("message.getTo(): {}", message.getTo());
        if (message.getTo()!= null ||(message.getTo().getPlatform()!= null
                && !message.getTo().getPlatform().equalsIgnoreCase("logger"))/*message.getTo().getPlatform()!= null && message.getTo().getPlatform().toLowerCase().contains("android")*/) {
            logger.info("creating FcmNotificationSender");
            sender = new FcmNotificationSender();
        }
        else {
            logger.info("Creating LoggerNotificationSender");
            sender = new LoggerNotificationSender();
        }

        return sender;
    }
}
