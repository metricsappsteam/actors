/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 08/02/2016.
 */
public class MongoQueryTranslatorTest {

    class TriggerCreateParams {

    }

    class LocationCreateParams {

    }

    @Test
    public void translateQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query)AbstractQueriesTest.getQueryStatement();
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query)AbstractQueriesTest.getQuerySelectStatement2Filters();
        translateAndCheck(q);
    }
    @Test
    public void translateSelectQueryLocation() throws Exception {
        AbstractQueries.Query q = AbstractQueriesTest.getQuerySelectStatementForAppIdOnLocations("test_application", TriggerCreateParams.class);
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQueryLocationWithElements() throws Exception {
        AbstractQueries.Query q = AbstractQueriesTest.getQuerySelectStatementForAppIdOnLocationsOnElements("test_application", LocationCreateParams.class);
        translateAndCheck(q);
    }


    @Test
    public void translateSelectQueryById() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query)AbstractQueriesTest
                .getQuerySelectStatementRangeFilterAndIdFilter("test_application", "location", TriggerCreateParams.class);
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQueryByIdLimit50() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query)AbstractQueriesTest
                .getQuerySelectStatementRangeFilterAndIdFilter("test_application", "location", TriggerCreateParams.class);
        q.limit(50);
        translateAndCheck(q);
    }

    private void checkHoldersAndParams(QueryStatementTranslator.QueryDef query) {
        int matches = StringUtils.countMatches(query.getFullQuery(), "#");
        assertEquals(matches, query.getQueryParams().size());
    }


    @Test
    public void translateRangeQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query)AbstractQueriesTest.getQuerySelectStatementRangeFilterAndEqual(TriggerCreateParams.class);
        translateAndCheck(q);
    }

    public void translateAndCheck(AbstractQueries.Query q) {

        MongoQueryTranslator translator = new MongoQueryTranslator();
        translator.setRootProperty("entity");

        translator.addPropertyRenderInterceptor(new QueryStatementTranslator.PropertyTemplateRenderInterceptor() {
            @Override
            public String renderPropertyTemplate(QueryStatementTranslator translator, String propertyName) {
                if (propertyName.equalsIgnoreCase(PersistenceMessageUtils.APPLICATION_ID) || propertyName.equalsIgnoreCase("_id")){
                    //render it simple .. dont include a root property whatsoever
                    return "%s";
                }
                return null;
            }
        });

      /*  translator.addPropertyRenderInterceptor(new QueryStatementTranslator.PropertyValueRenderInterceptor() {
            @Override
            public String renderPropertyValue(QueryStatementTranslator translator, String propertyName, Object propertyValue) {
                if (propertyName.equalsIgnoreCase("_id")){
                    //render it simple .. dont include a root property whatsoever
                    return "{$oid:\"" + propertyValue + "\"}";
                }
                return null;
            }

        });
*/
        QueryStatementTranslator.QueryTranslatorResult queryTranslatorResult = translator.translateQuery(q);
        QueryStatementTranslator.QueryDef query = queryTranslatorResult.getQuery();
        assertTrue(!query.getFullQuery().isEmpty());
        checkHoldersAndParams(query);

        System.out.println(String.format("Query: %s", query.getFullQuery()));
    }


}