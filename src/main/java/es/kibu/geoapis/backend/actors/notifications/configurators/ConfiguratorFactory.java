/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.configurators;

import akka.actor.ActorRef;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;

import java.util.HashMap;
import java.util.Map;

import static es.kibu.geoapis.backend.actors.notifications.NotificationMessage.NotificationType.PUSH;

/**
 * Created by lrodr_000 on 25/04/2016.
 */
public class ConfiguratorFactory {

    static Map<NotificationMessage.NotificationType ,  SenderConfigurator > registry = new HashMap<>();

    public static SenderConfigurator createConfigurator(NotificationMessage message, ActorRef actorRef){

        NotificationMessage.NotificationType type = message.getType();

        //if the registry already have on it will be returned.
        if (registry.containsKey(type)) {return registry.get(type);}

        switch (type){
            case SMS:
                break;
            case PUSH:
                return new GcmConfigurator();
            case CALL_URL:
                return new VoidConfigurator();
            case ACTOR:
                return new ActorConfigurator(actorRef);
        }

        throw new RuntimeException(String.format("No configurator created yet.. for this type of message", type));

    }

    public static void registerConfigurator(NotificationMessage.NotificationType messageType,  SenderConfigurator configurator) {
        registry.put(messageType, configurator);
    }
}
