package es.kibu.geoapis.backend.actors.messages;

/**
 * Created by lrodriguez2002cu on 15/02/2017.
 */
public class IdentifiableMessage implements Message, Identifiable<String> {

    String messageId;

    @Override
    public String getId() {
        return messageId;
    }

    @Override
    public void setId(String id) {
        throw new RuntimeException("Message id can not be modified");
      //this.messageId = id;
    }

    protected IdentifiableMessage() {
        messageId = String.format("message-"+ System.identityHashCode(this) );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentifiableMessage that = (IdentifiableMessage) o;

        return messageId != null ? messageId.equals(that.messageId) : that.messageId == null;
    }

    @Override
    public int hashCode() {
        return messageId != null ? messageId.hashCode() : 0;
    }
}
