/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors;

import akka.actor.ActorSystem;

/**
 * Created by lrodr_000 on 27/11/2015.
 */
public class MainActorSystem {

    private static ActorSystem actorSystem = null;

    public static ActorSystem getActorSystem(){
        if (actorSystem  == null || actorSystem.isTerminated()) {
            actorSystem = ActorSystem.create();
        }
        return actorSystem;
    }

    public static boolean isStarted(){
        return actorSystem  != null;
    }

    public static void terminateAndWait(){
        //ActorSystem actorSystem = getActorSystem();
        if (actorSystem != null) {
            actorSystem.shutdown();
            actorSystem.awaitTermination();
            actorSystem = null;
        }
    }

}
