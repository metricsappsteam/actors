/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import akka.util.Timeout;
import es.kibu.geoapis.backend.actors.ActorsUtils;
import es.kibu.geoapis.backend.actors.messages.*;
import org.apache.commons.lang3.RandomUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Class for testing the PersistenceMongoActor
 */

public class PersistenceMongoActorTest {
/*

    static ActorSystem system;

    static ApplicationCreateSecurityDetails testApplication;

     static UsersManager.UsersCreateDetails user;

    Logger logger = LoggerFactory.getLogger(PersistenceMongoActorTest.class);

    @BeforeClass
    public static void setup() {
        */
/*system = GeoFencesServer.startSystem();

        ApplicationsManager instance = DefaultApplicationsManager.getInstance();
        ApplicationsCreateParams adminApplication = instance.getAdminApplication();

        UsersCreateParams aUserCreatedForApp = ApplicationTestsUtils.getAUserCreatedForApp(adminApplication.client_id);

        ApplicationTestsUtils.SampleApplicationDataCreator sampleApplicationDataCreator =
                new ApplicationTestsUtils.SampleApplicationDataCreator().invoke("PMA.setup()");

        String description = "Test application for testing PersistentMongoActor";
        boolean c2dm = sampleApplicationDataCreator.isC2dm();
        boolean apns_dev = sampleApplicationDataCreator.isApns_dev();
        String name = sampleApplicationDataCreator.getName();
        String uri = sampleApplicationDataCreator.getUri();
        String iconUri = sampleApplicationDataCreator.getIconUri();

        testApplication = ApplicationTestsUtils.createApplication(instance, description, c2dm, apns_dev, name, uri, iconUri, aUserCreatedForApp.getId());
        user = TestUtils.createUser(new ApplicationId(testApplication.getApplication().client_id),
                "sample_user", "sample_password");*//*

    }


    @AfterClass
    public static void teardown() {

      */
/*  String userId = user.getUsersCreateParams().getId();
        //UserId userId = new UserId(userId);
        ApplicationId applicationId = new ApplicationId(testApplication.getApplication().client_id);

        UsersManager manager = UsersManagerFactory.getUsersManager();
                manager.deleteUser(userId, applicationId);


        ApplicationsManager applicationsManager = DefaultApplicationsManager.getInstance();
        applicationsManager.deleteApplication(applicationId);

        //shut down the system
        JavaTestKit.shutdownActorSystem(system);
        system = null;*//*

    }

    @Test
    public void testIt() {
        new JavaTestKit(system) {{
            final Props props = PersistenceMasterActor.props();
            final ActorRef subject = system.actorOf(PersistenceMongoActor.props());
            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            new Within(duration("1 seconds")) {
                protected void run() {

                    AbstractQueries.QueryStatement q = AbstractQueriesTest.getQuerySelectStatementForAppIdOnLocations("test_application");

                    subject.tell(q, getRef());

                    expectMsgClass(Result.class);

                    expectNoMsg();
                }
            };
        }};
    }

    interface QueryResultValidator{
        void validateResult(Result resultMessage);
    }

    abstract class DefaultQueryResultValidator implements QueryResultValidator{

        @Override
        public void validateResult(Result resultMessage) {
            PersistenceActor.QueryResult queryResult = PersistenceMessageUtils.resultToQueryResult(resultMessage);
            validateQueryResult(queryResult);
        }

        protected abstract void validateQueryResult(PersistenceActor.QueryResult queryResult);
    }


    public void testQuery(AbstractQueries.QueryStatement q, QueryResultValidator validator) {
        new JavaTestKit(system) {{
            final Props props = PersistenceMasterActor.props();
            final ActorRef subject = system.actorOf(PersistenceMongoActor.props());
            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            new Within(duration("1 seconds")) {

                protected void run() {

                    subject.tell(q, getRef());

                    Result result = expectMsgClass(Result.class);

                    validator.validateResult(result);

                    expectNoMsg();
                }
            };
        }};
    }


    public static AbstractQueries.Query  getHistorySelectionQuery(ApplicationId applicationId, UserId userId, int limit, String sortByProperty, AbstractQueries.SortDirection asc){
        AbstractQueries.Query query = new AbstractQueries.SelectQuery(LocationUpdateParams.class, "location");
        query.withFilter(PersistenceMessageUtils.applicationIdFilter(applicationId))
                .withFilter(PersistenceMessageUtils.userIdFilter(userId))
                .limit(limit).sortBy(sortByProperty, asc).build();
        return query;
    }


    public static AbstractQueries.Query  getHistorySelectionQueryAsc(ApplicationId applicationId, UserId userId, int limit, String sortByProperty){
        AbstractQueries.Query query = getHistorySelectionQuery(applicationId, userId, limit, sortByProperty, AbstractQueries.SortDirection.ASC);
        return query;
    }


    @Test
    public void testLocationsWithLimitAndOrderAsc() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();
        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();
        List<Identifiable<String>> createdObjectsIds = new ArrayList<>();
        String applicationId = testApplication.getApplication().client_id;
        String userId = user.getUsersCreateParams().getId();

        if (locationsHistoryEntering1000m.size()>0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);

            LocationHistory history = untilReachHistory.getHistory();

            for (Location location : history) {
                LocationUpdateParams locationUpdateParams = location.toLocationUpdateParams();
                AbstractQueries.Query createQuery = getLocationInsertQuery(applicationId, userId, locationUpdateParams);

                Identifiable<String> identifiable = new IdentityObject();
                identifiable  = (Identifiable<String>)PersistenceMessageUtils.performCreation(identifiable,createQuery, ActorsUtils.getPersistenceActor(), new Timeout(5, TimeUnit.SECONDS));
                createdObjectsIds.add(identifiable);
            }


            //make queries with some limits
            final int limit = RandomUtils.nextInt(history.size()) */
/*history.size() - 1*//*
;

            logger.debug("Testing limit with: {}", limit );
            AbstractQueries.QueryStatement queryStatement = getHistorySelectionQueryAsc(
                    new ApplicationId(applicationId), new UserId(userId), limit, "entity.minDate");


            testQuery(queryStatement, new DefaultQueryResultValidator() {
                @Override
                protected void validateQueryResult(PersistenceActor.QueryResult queryResult) {
                    List<LocationUpdateParams> locationUpdateParamResults = PersistenceMessageUtils.getRecords(queryResult);

                    Assert.assertTrue(locationUpdateParamResults.size() > 0);
                    Assert.assertTrue(locationUpdateParamResults.size() <= limit);

                    Assert.assertTrue(isSorted(locationUpdateParamResults));
                    logger.info("results: {} list: {}", locationUpdateParamResults.size(), locationUpdateParamResults);
                }
            });


        }

    }

    @Test
    public void testLocationsWithLimitAndOrder() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();
        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();
        List<Identifiable<String>> createdObjectsIds = new ArrayList<>();
        String applicationId = testApplication.getApplication().client_id;
        String userId = user.getUsersCreateParams().getId();

        if (locationsHistoryEntering1000m.size()>0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);

            LocationHistory history = untilReachHistory.getHistory();

            for (Location location : history) {
                LocationUpdateParams locationUpdateParams = location.toLocationUpdateParams();
                AbstractQueries.Query createQuery = getLocationInsertQuery(applicationId, userId, locationUpdateParams);

                Identifiable<String> identifiable = new IdentityObject();
                identifiable  = (Identifiable<String>)PersistenceMessageUtils.performCreation(identifiable,createQuery, ActorsUtils.getPersistenceActor(), new Timeout(5, TimeUnit.SECONDS));
                createdObjectsIds.add(identifiable);
            }


            //make queries with some limits
            final int limit = RandomUtils.nextInt(history.size()) */
/*history.size() - 1*//*
;

            logger.debug("Testing limit with: {}", limit );
            AbstractQueries.QueryStatement queryStatement = getHistorySelectionQuery(
                    new ApplicationId(applicationId), new UserId(userId), limit, "entity.points.*.date", AbstractQueries.SortDirection.ASC);


            testQuery(queryStatement, new DefaultQueryResultValidator() {
                @Override
                protected void validateQueryResult(PersistenceActor.QueryResult queryResult) {
                    List<LocationUpdateParams> locationUpdateParamResults = PersistenceMessageUtils.getRecords(queryResult);

                    Assert.assertTrue(locationUpdateParamResults.size()  > 0);
                    Assert.assertTrue(locationUpdateParamResults.size()  <= limit);

                    //Collections.shuffle(locationUpdateParamResults);
                    Assert.assertTrue(isSorted(locationUpdateParamResults));
                }
            });


        }

    }

    @Test
    public void testLocationsWithLimitAndOrder1() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();
        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();
        List<Identifiable<String>> createdObjectsIds = new ArrayList<>();
        String applicationId = testApplication.getApplication().client_id;
        String userId = user.getUsersCreateParams().getId();

        if (locationsHistoryEntering1000m.size()>0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);

            //desorganize the history
            LocationHistory history = untilReachHistory.getHistory().shuffle();


            for (Location location : history) {
                LocationUpdateParams locationUpdateParams = new InternalLocationUpdateParams(location.toLocationUpdateParams());
                AbstractQueries.Query createQuery = getLocationInsertQuery(applicationId, userId, locationUpdateParams);

                Identifiable<String> identifiable = new IdentityObject();
                identifiable  = (Identifiable<String>)PersistenceMessageUtils.performCreation(identifiable,createQuery, ActorsUtils.getPersistenceActor(), new Timeout(5, TimeUnit.SECONDS));
                createdObjectsIds.add(identifiable);
            }


            //make queries with some limits
            final int limit = RandomUtils.nextInt(history.size()) */
/*history.size() - 1*//*
;

            logger.debug("Testing limit with: {}", limit );
            AbstractQueries.QueryStatement queryStatement = getHistorySelectionQuery(
                    new ApplicationId(applicationId), new UserId(userId), limit, "entity.minDate", AbstractQueries.SortDirection.ASC);


            testQuery(queryStatement, new DefaultQueryResultValidator() {
                @Override
                protected void validateQueryResult(PersistenceActor.QueryResult queryResult) {
                    List<LocationUpdateParams> locationUpdateParamResults = PersistenceMessageUtils.getRecords(queryResult);

                    Assert.assertTrue(locationUpdateParamResults.size()  > 0);
                    Assert.assertTrue(locationUpdateParamResults.size()  <= limit);

                    //Collections.shuffle(locationUpdateParamResults);
                    Assert.assertTrue(isSorted(locationUpdateParamResults));
                }
            });

            AbstractQueries.QueryStatement queryStatement1 = getHistorySelectionQuery(
                    new ApplicationId(applicationId), new UserId(userId), limit, "entity.maxDate", AbstractQueries.SortDirection.ASC);


            testQuery(queryStatement1, new DefaultQueryResultValidator() {
                @Override
                protected void validateQueryResult(PersistenceActor.QueryResult queryResult) {
                    List<LocationUpdateParams> locationUpdateParamResults = PersistenceMessageUtils.getRecords(queryResult);

                    Assert.assertTrue(locationUpdateParamResults.size()  > 0);
                    Assert.assertTrue(locationUpdateParamResults.size()  <= limit);

                    //Collections.shuffle(locationUpdateParamResults);
                    Assert.assertTrue(isSorted(locationUpdateParamResults));
                }
            });



        }

    }


    private boolean isSorted(List<LocationUpdateParams> locationUpdateParamResults) {
        return TestUtils.isSorted(locationUpdateParamResults);
    }


    @Test
    public void testLocationsWithLimit() throws Exception {

        HistoryDataProducer producer = new HistoryDataProducer();
        producer.init();
        List<LocationHistory.UntilReachHistory> locationsHistoryEntering1000m = producer.getLocationsHistoryEntering1000m();
        List<Identifiable<String>> createdObjectsIds = new ArrayList<>();
        String applicationId = testApplication.getApplication().client_id;
        String userId = user.getUsersCreateParams().getId();

        if (locationsHistoryEntering1000m.size()>0) {
            //preparation phase
            LocationHistory.UntilReachHistory untilReachHistory = locationsHistoryEntering1000m.get(0);

            LocationHistory history = untilReachHistory.getHistory();

            for (Location location : history) {
                LocationUpdateParams locationUpdateParams = location.toLocationUpdateParams();
                AbstractQueries.Query createQuery = getLocationInsertQuery(applicationId, userId, locationUpdateParams);

                Identifiable<String> identifiable = new IdentityObject();
                identifiable  = (Identifiable<String>)PersistenceMessageUtils.performCreation(identifiable,createQuery, ActorsUtils.getPersistenceActor(), new Timeout(5, TimeUnit.SECONDS));
                createdObjectsIds.add(identifiable);
            }


            //make queries with some limits
            final int limit = RandomUtils.nextInt(history.size()) */
/*history.size() - 1*//*
;

            logger.debug("Testing limit with: {}", limit );
            AbstractQueries.QueryStatement queryStatement = QueriesUtils.filterLocationsWithLimit(
                    new ApplicationId(applicationId), new UserId(userId), limit);
            testQuery(queryStatement, new DefaultQueryResultValidator() {
                @Override
                protected void validateQueryResult(PersistenceActor.QueryResult queryResult) {
                    List<LocationUpdateParams> locationUpdateParamResults = PersistenceMessageUtils.getRecords(queryResult);
                    Assert.assertTrue(locationUpdateParamResults.size()  == limit);
                }
            });


        }

    }

    public AbstractQueries.Query getLocationInsertQuery(String applicationId, String userId, LocationUpdateParams locationUpdateParams) {
        return TestUtils.getLocationInsertQuery(applicationId, userId, locationUpdateParams);
    }
*/

}