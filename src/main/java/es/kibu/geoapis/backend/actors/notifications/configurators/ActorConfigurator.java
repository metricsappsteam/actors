/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.configurators;

import akka.actor.ActorRef;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.senders.ActorNotificationSender;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;

/**
 * Created by lrodr_000 on 29/04/2016.
 */
public class ActorConfigurator implements SenderConfigurator {

    private ActorRef senderRef;

    public ActorConfigurator(ActorRef sender) {
        if (sender == null) throw  new RuntimeException("Invalid sender actor reference. Reference can not be null");
        this.senderRef = sender;
    }

    public ActorRef getSenderRef(){
        return senderRef;
    }

    @Override
    public void configure(NotificationSender sender, NotificationMessage message) {
        if (sender instanceof ActorNotificationSender){
            ((ActorNotificationSender) sender).setSender(this.getSenderRef());
        }
    }
}
