/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.messages;


import es.kibu.geoapis.backend.actors.notifications.recipients.Recipient;

/**
 * Created by lrodriguez2002cu on 25/04/2016.
 * Holds the data related to the notifications, such as the application, session, user.
 */
public interface ApplicationRecipientData extends Recipient {

    /**
     * The user recipient.
     * @return
     */
    String getUser();

    /**
     * The session of the recipient
     * @return
     */
    String getSession();

    /**
     * A guuid
     * @return
     */
    String getUUID();

    /**
     * The version
     * @return
     */
    String getVersion();

    /**
     * A tag
     * @return
     */
    String getTag();
    /**
     * The category under which the message
     * It holds the applicationId
    **/
    String getApplication();
}
