/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.messages;

import java.io.Serializable;

/**
 * Class for representing different response types.
 * @param <T>
 */

public class Result<T extends Serializable> implements ResultMessage {

    @Override
    public boolean isSuccess() {
        return status == ResultType.ok;
    }

    public enum ResultType {ok, error};

    private String message;
    private T resultValue;

    private ResultType status;

    public String getMessage() {
        return message;
    }

    public Result(ResultType status, String message) {
        this.status = status;
        this.message = message;
    }

    public Result(ResultType status, String message, T result) {
        this(status,message);
        this.resultValue = result;
    }

    public Result(ResultType status, T result) {
        this.status = status;
        this.resultValue = result;
    }

    public ResultType getStatus() {
        return status;
    }

    public T getResultValue(){
        return resultValue;
    }
}
