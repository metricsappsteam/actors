package es.kibu.geoapis.backend.actors.messages;

import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodr_000 on 15/02/2017.
 */
public class IdentifiableMessageTest extends TestCase {

    Logger logger = LoggerFactory.getLogger(IdentifiableMessageTest.class);

    public void testGetId() throws Exception {
        IdentifiableMessage message = new IdentifiableMessage();
        assertTrue(message.getId() != null);
        logger.debug(message.getId());
    }

    public void testSetId() throws Exception {
        IdentifiableMessage message = new IdentifiableMessage();
        try {
            message.setId("this spould break");
            fail();
        } catch (Exception e) {

        }
    }

    public void testEquals() throws Exception {

        IdentifiableMessage message = new IdentifiableMessage();
        IdentifiableMessage message1 = new IdentifiableMessage();

        assertTrue(message.equals(message));
        assertFalse(message.equals(message1));
    }

    public void testHashCode() throws Exception {

        IdentifiableMessage message = new IdentifiableMessage();
        IdentifiableMessage message1 = new IdentifiableMessage();

        assertTrue(message.hashCode() == message.hashCode());
        assertTrue(message.hashCode() != message1.hashCode());
    }

}