/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.ActorRef;
import akka.util.Timeout;
import es.kibu.geoapis.backend.actors.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;

import static akka.pattern.Patterns.ask;

/**
 * Created by lrodriguez2002cu on 23/12/2015.
 */
public class PersistenceMessageUtils {

    public static final String APPLICATION_ID = PersistenceActor.APPLICATION_ID;
    public static final String USER_ID = PersistenceActor.USER_ID;
    public static final String ID = "_id";

    static Logger logger = LoggerFactory.getLogger(PersistenceMessageUtils.class);


    /**
     * Creates a CRUD message by overriding (copying and then) the context target and action.
     *
     * @param target      the target (in this context refers to the collection)
     * @param action      (not required, but if for some reason needs to be spedified)
     * @param crudCmd     (the crud command)
     * @param contextInfo (the context info that is gonna be overriden)
     * @return a valid crud message
     */

    public static CommandMessage getCRUDMessageOverrideContext(String target, String action, CRUDCommands crudCmd,
                                                               ContextInfo contextInfo) {
        ContextInfo context = contextInfo.clone();
        context.setCommand(new ContextInfo.ActionCommand(target, action));
        return new CommandMessage(crudCmd, context);
    }

    /**
     * Creates a CRUD message by overriding (copying and then) the context target and action.
     *
     * @param target      the target (in this context refers to the collection)
     * @param crudCmd     (the crud command)
     * @param contextInfo (the context info that is gonna be overriden)
     * @return a valid crud message
     */
    public static CommandMessage getCRUDMessageOverrideContext(String target, CRUDCommands crudCmd, ContextInfo contextInfo) {
        return getCRUDMessageOverrideContext(target, "", crudCmd, contextInfo);
    }


    public static AbstractQueries.Query getSelectAllQueryForApp(String target, Class resultClass, ApplicationId applicationId) {
        AbstractQueries.SelectQuery selectQuery = new AbstractQueries.SelectQuery(resultClass, target);
        selectQuery.withFilter(
                applicationIdFilter(applicationId)).build();
        return selectQuery;
    }

    public static AbstractQueries.QueryFilter applicationIdFilter(ApplicationId applicationId) {
        if (applicationId != null) {
            return new AbstractQueries.QueryFilter(APPLICATION_ID,
                    AbstractQueries.ComparisonOperators.EQUAL, applicationId.getId());
        } else return null;
    }

    public static AbstractQueries.QueryFilter entityIdFilter(String idField, EntityId entityId) {
        return new AbstractQueries.QueryFilter(idField, AbstractQueries.ComparisonOperators.WITH_ID, entityId.getId()/*"56b9db15b8c42800e4986c26"*/);
    }

    public static AbstractQueries.Query queryWithId(AbstractQueries.Query resultQuery, EntityId entityId) {
        if (entityId != null) {
            resultQuery.withFilter(PersistenceMessageUtils.entityIdFilter(ID, entityId));
        }
        return resultQuery;
    }

    public static AbstractQueries.Query queryWithUserId(AbstractQueries.Query resultQuery, UserId userId) {
        if (userId != null) {
            AbstractQueries.QueryFilter userFilter = userIdFilter(userId);
            resultQuery.withFilter(userFilter);
        }
        return resultQuery;
    }

    public static AbstractQueries.Query queryWithApplicationId(AbstractQueries.Query resultQuery, ApplicationId applicationId) {
        if (applicationId != null) {
            AbstractQueries.QueryFilter userFilter = applicationIdFilter(applicationId);
            resultQuery.withFilter(userFilter);
        }
        return resultQuery;
    }

    public static AbstractQueries.QueryFilter userIdFilter(UserId userId) {
        AbstractQueries.QueryFilter userFilter = null;
        if (userId != null) {
            userFilter = new AbstractQueries.QueryFilter(new AbstractQueries.EntityProperty(USER_ID),
                    AbstractQueries.ComparisonOperators.EQUAL, new AbstractQueries.QueryValue(userId.getId()));
        }
        return userFilter;
    }


    public static AbstractQueries.Query createInsertQuery(Object entity, String target, ApplicationId applicationId, UserId userId) {

        AbstractQueries.Query resultQuery = new AbstractQueries.InsertQuery(entity, target);
        AbstractQueries.QueryFilter applicationFilter = applicationIdFilter(applicationId);
        resultQuery.withFilter(applicationFilter);
        queryWithUserId(resultQuery, userId);

        return resultQuery;
    }

    public static boolean isError(PersistenceActor.QueryResult result) {
        return result instanceof PersistenceActor.DBExceptionResult;
    }

    public static String getErrorMsg(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBExceptionResult) {
            return ((PersistenceActor.DBExceptionResult) result).message;
        } else return null;
    }


    public static boolean isInsert(PersistenceActor.QueryResult result) {
        return result instanceof PersistenceActor.DBWriteResult;
    }

    public static boolean isDelete(PersistenceActor.QueryResult result) {
        return result instanceof PersistenceActor.DBDeleteResult;
    }


    public static PersistenceActor.QueryResult resultToQueryResult(Object rawResult) {
        if (rawResult instanceof Result) {
            return (PersistenceActor.QueryResult) ((Result) rawResult).getResultValue();
        }
        throw new RuntimeException(String.format("The rawResult provided is not of type Result, provieed (%s)", rawResult.getClass()));
    }

    public static <T> T getRecordsFromRawResult(Object rawResult) {
        PersistenceActor.QueryResult queryResult = PersistenceMessageUtils.resultToQueryResult(rawResult);
        T results = PersistenceMessageUtils.getRecords(queryResult);
        return results;
    }


   /* public static Object performCreation(Identifiable<String> identifiable, AbstractQueries.Query createQuery, ActorSelection persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, createQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = resultToQueryResult(rawResult);

            if (!PersistenceMessageUtils.isError(result) && PersistenceMessageUtils.isInsert(result) ) {
                String id = PersistenceMessageUtils.getInsertId(result);
                identifiable.setId(id);
                return identifiable;
            }
        } catch (Exception e) {
            logger.error("Error occurred while executing performCreation", e);
        }
        return null;
    }*/

    public static Object performCreate(Identifiable<String> identifiable, AbstractQueries.Query createQuery, ActorRef persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, createQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = resultToQueryResult(rawResult);

            if (!PersistenceMessageUtils.isError(result) && PersistenceMessageUtils.isInsert(result)) {
                String id = PersistenceMessageUtils.getInsertId(result);
                identifiable.setId(id);
                return identifiable;
            }
            else return result;
        } catch (Exception e) {
            logger.error("Error occurred while executing performCreation", e);
            throw new PersistenceException("Error occurred while executing performCreation", e);
        }
        //return null;
    }

    public static Object performUpdate(/*Identifiable<String> identifiable, */AbstractQueries.Query updateQuery, ActorRef persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, updateQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = resultToQueryResult(rawResult);

            //if (!PersistenceMessageUtils.isError(result) && PersistenceMessageUtils.isUpdate(result)) {
                return result;
            /*}
            else throw  new PersistenceException(PersistenceMessageUtils.getErrorMsg(
                    result));
        }
        catch(PersistenceException pe) {
            throw pe;*/
        }
        catch (Exception e) {
            logger.error("Error occurred while executing performUpdate", e);
            throw new PersistenceException("Error occurred while executing performUpdate", e);
        }
    }

    public static PersistenceActor.QueryResult performSelect(AbstractQueries.Query selectQuery, ActorRef persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, selectQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = resultToQueryResult(rawResult);

            //if (!PersistenceMessageUtils.isError(result)) {
                return result;
            /*}
            else throw new PersistenceException(PersistenceMessageUtils.getErrorMsg(result));
        }
        catch (PersistenceException pe) {
            throw pe;*/
        }
        catch (Exception e) {
            logger.error("Error occurred while executing performSelect", e);
            throw new PersistenceException("Error occurred while executing performSelect", e);
        }
    }

    public static boolean isUpdate(PersistenceActor.QueryResult result) {
        return result instanceof PersistenceActor.DBUpdateResult;
    }


    public static String getInsertId(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBWriteResult) {
            return ((PersistenceActor.DBWriteResult) result).id;
        }
        return null;
    }

    public static boolean hasRecords(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBReadResult) {
            return ((PersistenceActor.DBReadResult) result).resultset.size() > 0;
        }
        return false;
    }

    public static boolean hasModifications(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBUpdateResult) {
            return ((PersistenceActor.DBUpdateResult) result).modified_objects > 0;
        }
        return false;
    }

    public static boolean hasOneModification(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBUpdateResult) {
            return ((PersistenceActor.DBUpdateResult) result).modified_objects == 1;
        }
        return false;
    }

    public static int numberOfModifications(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBUpdateResult) {
            return ((PersistenceActor.DBUpdateResult) result).modified_objects;
        }
        return -1;
    }

    public static <T> T getUniqueRecord(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBReadResult) {
            PersistenceActor.DBReadResult dbReadResult = (PersistenceActor.DBReadResult) result;
            int size = dbReadResult.resultset.size();
            if (size == 1) {
                return (T) dbReadResult.resultset.get(0);
            } else {
                throw new PersistenceUtilsException(String.format("Unexpected number of records found (%d)", size));
            }
        } else throw new PersistenceUtilsException("The result param must be a result from a select query");
    }

    public static <T> T getRecords(PersistenceActor.QueryResult result) {
        if (result == null) throw new PersistenceUtilsException("The result param must not be null");

        if (result instanceof PersistenceActor.DBReadResult) {
            PersistenceActor.DBReadResult dbReadResult = (PersistenceActor.DBReadResult) result;
            return (T) dbReadResult.resultset;
        } else
            throw new PersistenceUtilsException(String.format("The result param must be a result from a select query, provided %s", result.getClass().getName()));
    }

    public static <T> T getFirstRecord(PersistenceActor.QueryResult result) {
        if (result instanceof PersistenceActor.DBReadResult) {
            PersistenceActor.DBReadResult dbReadResult = (PersistenceActor.DBReadResult) result;
            int size = dbReadResult.resultset.size();
            if (size > 0) {
                return (T) dbReadResult.resultset.get(0);
            } else {
                throw new PersistenceUtilsException(String.format("Unexpected number of records found (%d)", size));
            }
        } else throw new PersistenceUtilsException("The result param must be a result from a select query");
    }


    public static class PersistenceUtilsException extends RuntimeException {
        public PersistenceUtilsException(String s) {
            super(s);
        }
    }

/*
    public static boolean performDeletionQuery(AbstractQueries.Query deleteQuery, ActorSelection persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, deleteQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = PersistenceMessageUtils.resultToQueryResult(rawResult);
            if (!PersistenceMessageUtils.isError(result) && PersistenceMessageUtils.isDelete(result)) {
                return true;
            }
            else return false;

        } catch (Exception e) {
            logger.error("Error occurred while executing performDeletionQuery", e);
        }
        return false;
    }*/

    public static boolean performDeletionQuery(AbstractQueries.Query deleteQuery, ActorRef persistenceActor, Timeout timeout) {
        try {
            Future<Object> futureResult = ask(persistenceActor, deleteQuery, timeout);
            Object rawResult = Await.result(futureResult, timeout.duration());
            PersistenceActor.QueryResult result = PersistenceMessageUtils.resultToQueryResult(rawResult);
            return !PersistenceMessageUtils.isError(result) && PersistenceMessageUtils.isDelete(result);

        } catch (Exception e) {
            logger.error("Error occurred while executing performDeletionQuery", e);
        }
        return false;
    }
}
