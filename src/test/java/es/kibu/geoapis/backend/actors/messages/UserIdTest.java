/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.messages;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 21/04/2016.
 */
public class UserIdTest {

    @Test
    public void testApplicationId(){
        String testId = "TESST_ID";
        UserId userId = new UserId(testId);
        assertTrue(testId.equalsIgnoreCase(userId.getId()));
        assertFalse(userId.isUnassigned());
    }


}