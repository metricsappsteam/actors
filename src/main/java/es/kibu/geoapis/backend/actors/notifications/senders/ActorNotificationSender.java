/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.senders;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import es.kibu.geoapis.backend.actors.ActorsUtils;
import es.kibu.geoapis.backend.actors.notifications.NotificationMessage;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import es.kibu.geoapis.backend.actors.notifications.results.SimpleNotificationResult;

/**
 * Created by lrodr_000 on 25/04/2016.
 */
public class ActorNotificationSender implements NotificationSender {
    private ActorRef sender;

    public ActorNotificationSender(/*NotificationMessage message*/) {
    }

    public void setSender(ActorRef sender){
        this.sender = sender;
    }

    @Override
    public NotificationResult send(NotificationMessage notificationMessage) {
        try {
            String target = notificationMessage.getTo().getTarget();
            ActorSelection actorSelection = ActorsUtils.forPath(target);
            actorSelection.tell(notificationMessage, sender);
            return new SimpleNotificationResult(true);
        } catch (Exception e) {
            SimpleNotificationResult errorNotificationResult = new SimpleNotificationResult(false);
            errorNotificationResult.setData(e);
            return errorNotificationResult;
        }

    }
}
