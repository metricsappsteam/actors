package es.kibu.geoapis.backend.actors.messages;

/**
 * Created by lrodr_000 on 10/11/2016.
 */
public interface Identifiable<T> {
    T getId();
    void setId(T id);
}
