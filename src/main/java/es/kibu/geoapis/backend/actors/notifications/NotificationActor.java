/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import es.kibu.geoapis.backend.actors.notifications.configurators.ConfiguratorFactory;
import es.kibu.geoapis.backend.actors.notifications.configurators.SenderConfigurator;
import es.kibu.geoapis.backend.actors.notifications.results.NotificationResult;
import es.kibu.geoapis.backend.actors.notifications.senders.NotificationSender;
import es.kibu.geoapis.backend.actors.notifications.senders.SenderFactory;
import es.kibu.geoapis.backend.configuration.Settings;
import es.kibu.geoapis.backend.configuration.SettingsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.reflect.ClassTag;

/**
 * Created by lrodriguez2002cu on 14/12/2015.
 */
public class NotificationActor extends UntypedActor {

    public static final String NOTIFICATION_ACTOR = "NotificationActor";

    final SettingsImpl settings = Settings.SettingsProvider.get(getContext().system());

    LoggingAdapter logger = Logging.getLogger(getContext().system(), this);

    public static Props props() {
        ClassTag<NotificationActor> tag = scala.reflect.ClassTag$.MODULE$.apply(NotificationActor.class);
        return Props.apply(tag);
    }

    static ActorRef testActor;
    ResultInterceptor interceptor = new ResultInterceptor();

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ActorRef) {
            NotificationActor.testActor = (ActorRef) message;
        }

        if (message instanceof NotificationMessage) {
            logger.info("Receiving notification message {}", message);
            NotificationMessage notificationMessage = (NotificationMessage) message;

            logger.info("Creating the configurator");
            SenderConfigurator configurator = ConfiguratorFactory.createConfigurator(notificationMessage, self());

            logger.info("Creating the notification sender");
            NotificationSender sender = SenderFactory.createSender(notificationMessage, configurator);

            logger.info("Creating the notification sending message");
            NotificationResult sendResult = sender.send(notificationMessage);

            //testing stuff so ignore
            if (testActor != null) {
                testActor.tell(sendResult, self());
            }
            notifyResult(sendResult);
        } else unhandled(message);
    }

    public void notifyResult(NotificationResult sendResult) {
        interceptor.notifyResult(sendResult);
    }


    public static class ResultInterceptor {

        Logger logger = LoggerFactory.getLogger(ResultInterceptor.class);

        public void notifyResult(NotificationResult result) {
            logger.debug(String.format("The notification interceptor success = %s", Boolean.toString(result.isSuccess())));
        }

    }

}
