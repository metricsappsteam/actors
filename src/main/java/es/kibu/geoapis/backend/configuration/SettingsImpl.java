/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.configuration;

import akka.actor.Extension;
import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 04/12/2015.
 */

/*

myapp {
        db {
        uri = "mongodb://example1.com:27017,example2.com:27017"
        }
        circuit-breaker {
        timeout = 30 seconds
        }
        }

*/
public class SettingsImpl implements Extension {

    Logger logger = LoggerFactory.getLogger(SettingsImpl.class);

    public static String BaseKEY = "actors";
    public static final String GEOFENCES_BACKEND_TESTING_KEY = BaseKEY + ".testing";
    public final String DB_URI;
    public final String DB_NAME;
    public final String GCM_API_KEY;
    public final Duration SERVICES_REQUEST_MAX_DURATION;

    public final Duration CIRCUIT_BREAKER_TIMEOUT;

    public final String ADMIN_USER;
    public final String ADMIN_PASS;
    public final String ADMIN_EMAIL;


    private boolean isTesting(){
       return config.hasPath(GEOFENCES_BACKEND_TESTING_KEY) && config.getBoolean(GEOFENCES_BACKEND_TESTING_KEY);
    }

    private String getResource(String resource){
        String format = (isTesting())?"%s-testing":"%s";
        return String.format(format, resource);
    }

    Config config;

    public SettingsImpl(Config config) {
        this.config  = config;
        //db related
        BaseKEY = (config.hasPath("base"))? config.getString("base"): BaseKEY;

        logger.info("Loaded baseKey: {}", BaseKEY );

        DB_URI = config.getString(String.format(BaseKEY + ".%s.uri", getResource("db")));
        DB_NAME = config.getString(String.format(BaseKEY + ".%s.name", getResource("db")));

        logger.info("Loaded DB_URI: {}", DB_URI);
        logger.info("Loaded DB_NAME: {}", DB_NAME);

        String notificationsPath = BaseKEY + ".notifications.gcm-api-key";

        GCM_API_KEY = (config.hasPath(notificationsPath))?config.getString(BaseKEY + ".notifications.gcm-api-key"):"";

        logger.info("Loaded GCM_API_KEY: {}", GCM_API_KEY);
        //services related
        String servicesPath = BaseKEY + ".rest-services.max-request-duration";
        SERVICES_REQUEST_MAX_DURATION =  config.hasPath(servicesPath)?Duration.create(config.getDuration(servicesPath, TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS)
                : Duration.create(500, TimeUnit.MILLISECONDS);


        String breakerTimeout = BaseKEY + ".circuit-breaker.timeout";
        CIRCUIT_BREAKER_TIMEOUT =config.hasPath(servicesPath)?
                Duration.create(config.getDuration(breakerTimeout,
                        TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS): Duration.create(1000, TimeUnit.MILLISECONDS);

        String adminPath = BaseKEY + ".admin";
        if (config.hasPath(adminPath)){
            ADMIN_USER = config.getString(BaseKEY + ".admin.user");
            ADMIN_PASS = config.getString(BaseKEY + ".admin.password");
            ADMIN_EMAIL = config.getString(BaseKEY + ".admin.email");
        }
        else  {
            ADMIN_USER = "";
            ADMIN_PASS = "";
            ADMIN_EMAIL = "";
        }
    }

}