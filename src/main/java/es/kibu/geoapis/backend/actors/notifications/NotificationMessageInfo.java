/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;


import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 21/04/2016.
 */

/**
 * Defines the data to be included in the payload of the message.
 */
public class NotificationMessageInfo implements Serializable {

    FenceInfo layer;
    UserInfo user;
    String event;

    public FenceInfo getLayer() {
        return layer;
    }

    public void setLayer(FenceInfo layer) {
        this.layer = layer;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


    public NotificationMessageInfo(FenceInfo layer, UserInfo user, String event) {
        this.layer = layer;
        this.user = user;
        this.event = event;
    }

    /*

    {
 	"layer": {
 		"bounds": {
 			"center": {
 				"latitude": 45.521740823313,
 				"longitude": -122.68043538074
 			},
 			"map": "http://map.geoloqi.com/45.521752557438,-122.68041864527?radius=546.45073462403",
 			"ne": {
 				"latitude": 45.524569933375,
 				"longitude": -122.6764
 			},
 			"sw": {
 				"latitude": 45.5165,
 				"longitude": -122.68448239021
 			},
 			"radius": 548.7265
 		},
 		"description": "This is not the layer you're looking for.",
 		"extra": {},
 		"icon": "https://s3.amazonaws.com/geoloqi/layers/logo-57px.png",
 		"layer_id": "7ky",
 		"name": "Test Layer",
 		"public": 1,
 		"public_userlist": 0,
 		"settings": false,
 		"subscription": false,
 		"trigger_rate_limit": 30,
 		"type": "normal",
 		"user_id": "Gf"
 	},
 	"place": {
 		"active": 1,
 		"description": "",
 		"display_name": "Portland, OR",
 		"extra": {},
 		"latitude": 45.5165,
 		"longitude": -122.6764,
 		"radius": 200,
 		"map": "http://map.geoloqi.com/45.5165,-122.6764?radius=200",
 		"locality": {
 			"name": "Portland"
 		},
 		"region": {
 			"name": "OR"
 		},
 		"name": "",
 		"place_id": "2Np_",
 		"time_from": "00:00:00",
 		"time_to": "00:00:00"
 	},
 	"trigger": {
 		"callback_url": "http://requestb.in/1gt47ek1",
 		"extra": {},
 		"one_time": "0",
 		"place_id": "2Np_",
 		"trigger_after": "0",
 		"trigger_id": "2iav",
 		"trigger_on": "enter",
 		"type": "callback"
 	},
 	"triggered_delay": "0",
 	"event": "enter",
 	"user": {
 		"bio": "CTO, Esri R&D Center, Portland",
 		"devices": {
 			"apns_dev": [],
 			"apns_live": [],
 			"c2dm": [],
 			"gcm": []
 		},
 		"display_name": "Aaron Parecki",
 		"extra": {},
 		"has_custom_username": "1",
 		"has_push_token": "1",
 		"is_anonymous": "0",
 		"name": "Aaron Parecki",
 		"profile_image": "http://aaronparecki.com/images/aaronpk.png",
 		"public_geonote_email": "1",
 		"public_geonotes": "1",
 		"public_location": "0",
 		"timezone": "America/Los_Angeles",
 		"timezone_offset": "-0700",
 		"twitter": "aaronpk",
 		"user_id": "Gf",
 		"username": "aaronpk",
 		"website": "http://aaronparecki.com/"
 	}
 }


    * */

}
