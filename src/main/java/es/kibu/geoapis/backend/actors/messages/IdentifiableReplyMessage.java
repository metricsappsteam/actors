package es.kibu.geoapis.backend.actors.messages;

/**
 * Created by lrodr_000 on 15/02/2017.
 */
public class IdentifiableReplyMessage extends IdentifiableMessage {

    IdentifiableMessage requestMessage;

    public IdentifiableReplyMessage(IdentifiableMessage requestMessage) {
        this.requestMessage = requestMessage;
    }

    public IdentifiableMessage getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(IdentifiableMessage requestMessage) {
        this.requestMessage = requestMessage;
    }

    public boolean isReplyFor(IdentifiableMessage message){
        return requestMessage.equals(message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        IdentifiableReplyMessage that = (IdentifiableReplyMessage) o;

        return getRequestMessage() != null ? getRequestMessage().equals(that.getRequestMessage()) : that.getRequestMessage() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getRequestMessage() != null ? getRequestMessage().hashCode() : 0);
        return result;
    }
}
