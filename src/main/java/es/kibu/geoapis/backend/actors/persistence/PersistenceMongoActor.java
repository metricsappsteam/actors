/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.Props;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import es.kibu.geoapis.backend.actors.messages.CommandMessage;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.backend.actors.messages.Result;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.*;
import es.kibu.geoapis.backend.actors.serialization.SerializationUtils;
import org.bson.types.ObjectId;
import org.jongo.*;
import org.jongo.bson.Bson;
import org.jongo.bson.BsonDocument;
import org.jongo.marshall.Unmarshaller;
import org.jongo.marshall.jackson.JacksonMapper;
import scala.reflect.ClassTag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static es.kibu.geoapis.backend.actors.persistence.Targets.AUTHORIZATION;

@SuppressWarnings("Duplicates")
public class PersistenceMongoActor extends PersistenceActor {

    public static final String SERIALIZATION_ACTOR = "serializationMongoActor";
    private static final String ENTITY = "entity";
    private static final String ID_FIELD = "_id";
    private static final String ENTITY_ID = ID_FIELD;

    //Mongo client
    private Jongo jongo;

    /*The saved objects will be stored in this way in mongo, in order to be able to filter by the application id,
    * furtther metadata can be included*/
    public static class MongoEntity {

        //the id
        public ObjectId _id;

        //the entity
        public Object entity;

        //the application id to which the data belongs to
        public String applicationId;

        public String userId;
        /**
         * Creates a new entity
         * @param entity the entity to insert
         * @param applicationId the application Id over which to insert
         */
        public MongoEntity(Object entity, String applicationId, String userId) {
            this.entity = entity;
            this.applicationId = applicationId;
            this.userId = userId;
        }

    }


    public static Props props() {
        ClassTag<PersistenceMongoActor> tag = scala.reflect.ClassTag$.MODULE$.apply(PersistenceMongoActor.class);
        return Props.apply(tag);
    }

    private Map<String, MongoCollection> collectionMap = new HashMap<>();

    /**
     * Validates and creates a collection (if neeeded) out of the collection name provided
     * @param collection the collection name
     * @return the mongo collection object
     */
    private MongoCollection validateOrCreateCollection(String collection) {
        //check if it is already in de map of db collections
        boolean contained = collectionMap.containsKey(collection);

        //if not in the collection and the database collection does not exist
        if (!contained && !jongo.getDatabase().collectionExists(collection)) {

            //create the collection
            jongo.getDatabase().createCollection(collection, null);

            //add it to the dbcollection mp
            collectionMap.put(collection, jongo.getCollection(collection));

            //get the collection and add it
            return jongo.getCollection(collection);
        } else {

            if (!contained) {
                //if exists but not registered  in the map of collections
                MongoCollection dbcollection = jongo.getCollection(collection);

                //add it to the map
                collectionMap.put(collection, dbcollection);
                //return it
                return dbcollection;
            } else {
                //if was in the collection just return it
                return collectionMap.get(collection);
            }
        }
    }


    /**
     * Initializes the aspects related to the persistence of the objects in mongo
     */
    private void init() {
        try {
            //TODO: check how to fix this DB
            @SuppressWarnings("deprecation") DB db = new MongoClient(settings.DB_URI).getDB(settings.DB_NAME);

            //sets the visibility checker
            VisibilityChecker visibilityChecker = new SerializationUtils.GeofencesCustomVisibilityChecker(JsonAutoDetect.Visibility.PUBLIC_ONLY);
                   /*new VisibilityChecker.Std(JsonAutoDetect.Visibility.PUBLIC_ONLY)
                    .withVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE);*/

            //the mapper
            Mapper mapper = new JacksonMapper.Builder()
                    .setVisibilityChecker(visibilityChecker)
                    //.registerModule(new JodaModule())
                    .registerModule(new SerializationUtils.GeofencesSerializationModule())
                    .build();

            jongo = new Jongo(db, mapper);

        } catch (Exception e) {
            String message = e.getMessage();
            log.error(message);
        }
    }


/*    private void verifyInsertion(String target, Object entity){
        if (target.equalsIgnoreCase(Targets.LOCATION)) {
            //check that it is an internal
            if (!(entity instanceof InternalLocationUpdateParams)) {
                throw new RuntimeException("The entities for inserting on location collection");
            }
        }

    }*/

    @Override
    public void preStart() throws Exception {
        super.preStart();
        init();
    }

    public void onReceive(Object message) throws Exception {

        if (message instanceof CommandMessage) {

            String receivedMessage = String.format("%s ( %s )", ((CommandMessage) message).getCommand().verb(), ((CommandMessage) message).getData().toString());
            log.info("In PersistenceMongoActor - received message: {}", receivedMessage);

            //handle the message
            QueryResult result = handleMessage((CommandMessage) message);

            assert result != null;

            //reply to the sender
            getSender().tell(createResultMsg(result), getSelf());

        } else {

            // if the received message is a Query
            if (message instanceof Query) {

                Query query = (Query) message;

                log.info("In PersistenceMongoActor - received message: {}", query);

                //handle the message
                QueryResult result = handleQuery(query);

                assert result != null;

                //get the result type
                Result.ResultType status = result.getResultType();

                //reply to the sender
                getSender().tell(createResultMsg(result), getSelf());

            }
            else {
                unhandled(message);
            }
        }
    }


    protected String getEntityId(Query query){
        return getFilterProperty(query, ENTITY_ID);
    }

    protected String getApplicationId(Query query){
        return getFilterProperty(query, APPLICATION_ID);
    }

    protected String getUserId(Query query){
        return getFilterProperty(query, USER_ID);
    }

    @Override
    protected QueryResult doDelete(AbstractQueries.Query query) throws Exception {

        //some assertions
        assert query.getQueryType() == AbstractQueries.QueryType.DELETE;

        String id = getFilterProperty(query, ENTITY_ID);
        assert id != null;

        String applicationId = getApplicationId(query);
        assert applicationId != null;

        String  targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;

        MongoCollection collection = validateOrCreateCollection(targetCollection);

        //remove the object from the collection

        WriteResult result = collection.remove(withOidAndApplication(id, applicationId));
        return new DBDeleteResult(id,result.wasAcknowledged());
        //throw new FeatureNotSupportedException("doUpdate(Query query) is still not suported");
    }

    @Override
    protected QueryResult doUpdate(Query query) throws Exception {
        //some assertions
        assert query.getQueryType() == QueryType.UPDATE;

        String id = getEntityId(query);
        assert id != null;

        String applicationId = getApplicationId(query);
        assert applicationId != null;

        String  targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;


        MongoCollection collection = validateOrCreateCollection(targetCollection);

        //remove the object from the collection
        Object entity = ((UpdateQuery)query).getEntity();

        WriteResult result = collection.update(withOidAndApplication(id, applicationId)).with(entityFieldModifierSetTo(), entity);
        //,result.isUpdateOfExisting()
        /*n - the number of existing documents affected by this operation
        updateOfExisting - true if the operation was an update and an existing document was updated
        upsertedId - the _id of a document that was upserted by this operation*/

        return new DBUpdateResult(id, result.isUpdateOfExisting(), result.getN());
        //throw new FeatureNotSupportedException("doUpdate(Query query) is still not suported");
    }

    //Some constants that is better not to touch accidentally
    private String entityFieldModifierSetTo() {
        return "{$set: {entity: #}}";
    }

    private String withOidAndApplication(String id, String applicationId){
        return "{_id: {$oid:\"" + id + "\"}, applicationId: \""+applicationId +"\"}";
    }

    private String withProjectionOfEntity(){
        return "{entity : #, _id: #}";
    }


    /***
     * This is where the queries to the system occur.
     * @param query the query
     * @return a query result
     * @throws Exception
     */
    @Override
    protected QueryResult doSelection(Query query) throws Exception {

        //some assertions
        assert query.getQueryType() == QueryType.SELECT;

        /*String applicationId = getApplicationId(query);
        assert applicationId != null;*/

        String  targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;

        MongoCollection collection = validateOrCreateCollection(targetCollection);
        List<Object> list = new ArrayList<>();

        MongoCursor<?> mongoCursor;

        Class resultClass = ((SelectQuery)query).getResultClass();

        ResultHandler<Object> projectionHandler = dbObject -> {

            Unmarshaller unmarshaller = jongo.getMapper().getUnmarshaller();

            DBObject entity = (DBObject) dbObject.get(ENTITY);

            BsonDocument bsonDocument = Bson.createDocument(entity);

            Object unmarshalled = unmarshaller.unmarshall(bsonDocument, resultClass);

            //shall include the id?
            if (unmarshalled instanceof Identifiable){
                String id = dbObject.containsField(ID_FIELD)? dbObject.get(ID_FIELD).toString(): "NO_ID" ;
                ((Identifiable) unmarshalled).setId(id);
            }
            return unmarshalled;

        };


        QueryStatementTranslator.QueryDef queryDef = translateQuery(query);

        log.debug("Query: {}", queryDef.getFullQuery());
        Object[] parameters = queryDef.getQueryParams().toArray(/*new Object[0]*/);
        String queryText = queryDef.getFiltersPart();

        Find find = collection.find(queryText, parameters);
        if (query.getLimit()>0){
            find.limit(query.getLimit());
        }

        if (query.getSkip()>0){
           find.skip(query.getSkip());
        }

        if (query.getSorts()!= null && !query.getSorts().isEmpty() ){
            String sortExp = "";
            String sep = "";
            for (QuerySort querySort : query.getSorts()) {
                SortDirection direction = querySort.getDirection();
                String directionStr = (direction == SortDirection.ASC)? "1" : "-1";
                sortExp += sep + String.format("{%s:%s}", querySort.getProperty().getPropertyName(), directionStr);
            }

            find.sort(sortExp);
        }

        find.projection(withProjectionOfEntity(), 1, 1);

        mongoCursor = find.map(projectionHandler);

        try {
            log.info("retrieving {} elements of type {} from database", mongoCursor.count(), resultClass);
            while (mongoCursor.hasNext()) {
                list.add(mongoCursor.next());
            }
            mongoCursor.close();

        } catch (Exception e){
            //close the cursor.. always
            log.error(e, "Error while retrieving data from the cursor");
            return new DBExceptionResult(e);
        }

        return new DBReadResult(list);
    }

    private QueryStatementTranslator.QueryDef translateQuery(Query q) {
        MongoQueryTranslator translator = new MongoQueryTranslator();
        translator.setRootProperty("entity");

        translator.addPropertyRenderInterceptor(new QueryStatementTranslator.PropertyTemplateRenderInterceptor() {
            @Override
            public String renderPropertyTemplate(QueryStatementTranslator translator, String propertyName) {
                if (propertyName.equalsIgnoreCase(PersistenceMessageUtils.APPLICATION_ID)
                        || propertyName.equalsIgnoreCase(PersistenceMessageUtils.ID)
                        || propertyName.equalsIgnoreCase(PersistenceMessageUtils.USER_ID)) {
                    //render it simple .. dont include a root property whatsoever, except in the case of authorization
                    if (propertyName.equalsIgnoreCase(PersistenceMessageUtils.USER_ID)
                            && translator.getCurrentQuery().getTarget().getTargetName().equalsIgnoreCase(AUTHORIZATION))
                    {
                        return null;
                    }

                    return "%s";
                }
                return null;
            }
        });
        QueryStatementTranslator.QueryTranslatorResult queryTranslatorResult = translator.translateQuery(q);
        QueryStatementTranslator.QueryDef queryDef = queryTranslatorResult.getQuery();
        return queryDef;
    }

    /*private String withApplicationId() {
        return String.format("{%s : #}", PersistenceActor.APPLICATION_ID);
    }*/

    /**
     * Insertions happen here..
     * @param query the query containing the entity to insert
     * @return  the result.
     * @throws Exception
     */
    @Override
    protected QueryResult doInsertion(Query query) throws Exception {
        //few assertions
        assert query.getQueryType() == QueryType.INSERT;

        String applicationId = getApplicationId(query);
        //assert applicationId != null;

        String  targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null && !targetCollection.isEmpty();

        Object entity = ((InsertQuery) query).getEntity();
        assert entity != null;

        //if an insertion comes with an id.. it would be at the least suspicious
        assert getEntityId(query) == null;

        MongoCollection collection = validateOrCreateCollection(targetCollection);

        String userId = getUserId(query);

        //wrap the entity in a mongo entity
        MongoEntity mongoEntity = new MongoEntity(entity, applicationId, userId);

        WriteResult result = collection.save(mongoEntity);

        assert result.getUpsertedId() != null;

        log.info("saving object with id {}", result.getUpsertedId() == null ? "null" : result.getUpsertedId());

        return new DBWriteResult(result.getUpsertedId().toString());
    }
}