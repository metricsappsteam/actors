/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications;

import java.util.Map;

/**
 * Created by lrodr_000 on 22/04/2016.
 */
public class UserInfo {

    String bio; //"bio": "CTO, Esri R&D Center, Portland",
    DevicesInfo devices;
    String display_name;
    Map<String, String> extra;
    String has_custom_username;
    String has_push_token;
    String is_anonymous;
    String name;
    String profile_image;
    String public_geonote_email;
    String public_geonotes;
    String public_location;
    String timezone;
    String timezone_offset;
    String twitter;
    String user_id;
    String username;
    String website;

    public UserInfo(String bio, DevicesInfo devices, String display_name, Map<String, String> extra,
                    String has_custom_username, String has_push_token, String is_anonymous, String name,
                    String profile_image, String public_geonote_email, String public_geonotes, String public_location,
                    String timezone, String timezone_offset, String twitter, String user_id, String username, String website) {
        this.setBio(bio);
        this.setDevices(devices);
        this.setDisplay_name(display_name);
        this.setExtra(extra);
        this.setHas_custom_username(has_custom_username);
        this.setHas_push_token(has_push_token);
        this.setIs_anonymous(is_anonymous);
        this.setName(name);
        this.setProfile_image(profile_image);
        this.setPublic_geonote_email(public_geonote_email);
        this.setPublic_geonotes(public_geonotes);
        this.setPublic_location(public_location);
        this.setTimezone(timezone);
        this.setTimezone_offset(timezone_offset);
        this.setTwitter(twitter);
        this.setUser_id(user_id);
        this.setUsername(username);
        this.setWebsite(website);
    }

    public UserInfo() {
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public DevicesInfo getDevices() {
        return devices;
    }

    public void setDevices(DevicesInfo devices) {
        this.devices = devices;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Map<String, String> getExtra() {
        return extra;
    }

    public void setExtra(Map<String, String> extra) {
        this.extra = extra;
    }

    public String getHas_custom_username() {
        return has_custom_username;
    }

    public void setHas_custom_username(String has_custom_username) {
        this.has_custom_username = has_custom_username;
    }

    public String getHas_push_token() {
        return has_push_token;
    }

    public void setHas_push_token(String has_push_token) {
        this.has_push_token = has_push_token;
    }

    public String getIs_anonymous() {
        return is_anonymous;
    }

    public void setIs_anonymous(String is_anonymous) {
        this.is_anonymous = is_anonymous;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getPublic_geonote_email() {
        return public_geonote_email;
    }

    public void setPublic_geonote_email(String public_geonote_email) {
        this.public_geonote_email = public_geonote_email;
    }

    public String getPublic_geonotes() {
        return public_geonotes;
    }

    public void setPublic_geonotes(String public_geonotes) {
        this.public_geonotes = public_geonotes;
    }

    public String getPublic_location() {
        return public_location;
    }

    public void setPublic_location(String public_location) {
        this.public_location = public_location;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezone_offset() {
        return timezone_offset;
    }

    public void setTimezone_offset(String timezone_offset) {
        this.timezone_offset = timezone_offset;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
