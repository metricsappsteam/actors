/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.serialization;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;

/**
 * This class contains utilities classes relarted to the serialization of various types used in the backend server.
 * @author lrodrriguiez2002cu on 04/12/2015.
 */
public class SerializationUtils {


    public static class DateTimeSerializer extends JsonSerializer<DateTime> {
        @Override
        public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeObject(dateTime.toDate());
            //serializerProvider.defaultSerializeDateValue(dateTime.toDate(), jsonGenerator);
        }

    }

    public static class DateTimeDeserializer extends JsonDeserializer<DateTime> {
        @Override
        public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Date t = deserializationContext.readValue(jsonParser, Date.class);
            DateTime dt = new DateTime(t);
            return dt;
        }
    }


    /*public static class StringValueSerializer extends JsonSerializer<BaseParams.StringValue> {
        @Override
        public void serialize(BaseParams.StringValue stringValue, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(stringValue.getValue());
        }
    }

    public static class StringValueDeserializer extends JsonDeserializer<BaseParams.StringValue> {

        @Override
        public BaseParams.StringValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            BaseParams.StringValue value = new BaseParams.StringValue();
            value.setValue(jsonParser.getValueAsString());
            return value;
        }
    }*/

    /*public static class ExtrasDeserializer extends JsonDeserializer<BaseParams.Extras> {

        @Override
        public BaseParams.Extras deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            BaseParams.Extras value = new BaseParams.Extras();

            TypeReference reference = new TypeReference<HashMap<String,String>>() {};
            HashMap<String, String> map = jsonParser.readValueAs(reference);
            value.setValues(map*//*jsonParser.readValueAs(mapType)*//*);
            return value;
        }
    }
*/
  /*  public static class ExtrasSerializer extends JsonSerializer<BaseParams.Extras> {
        @Override
        public void serialize(BaseParams.Extras extras, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

            //jsonGenerator.writeArrayFieldStart();
            jsonGenerator.writeObject(extras.getValues());
            //jsonGenerator.writeString(stringValue.getValue());
        }
    }

    public static class TimeSerializer extends JsonSerializer<BaseParams.Time>*//*, com.google.gson.JsonDeserializer<BaseParams.Time>*//* {
        @Override
        public void serialize(BaseParams.Time time, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(time.toString());
        }

        *//*public BaseParams.Time deserialize(JsonElement json, Type typeOfT,
                                           JsonDeserializationContext context) throws JsonParseException {
            return new BaseParams.Time(json.getAsString());
        }
*//*
       *//* public JsonElement serialize(BaseParams.Time time, Type typeOfT,
                                     JsonSerializationContext context) {
            JsonElement e = new JsonPrimitive(time.toString());
            return e;
        }*//*

    }

    public static class TimeDeserializer extends JsonDeserializer<BaseParams.Time>*//*, com.google.gson.JsonDeserializer<BaseParams.Time>*//* {
        @Override
        public BaseParams.Time deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return new BaseParams.Time(jsonParser.getValueAsString());
        }
    }
*/
    public static class GeofencesSerializationModule extends SimpleModule
    {
        public GeofencesSerializationModule() {
            super("GeofencesSerializationModule"/*, new Version(0,0,1,null)*/);
            //context.setMixInAnnotations(Target.class, MixIn.class);
  /*          this.addSerializer(BaseParams.StringValue.class, new StringValueSerializer());
            this.addDeserializer(BaseParams.StringValue.class, new StringValueDeserializer());
            this.addSerializer(BaseParams.Time.class, new TimeSerializer());
            this.addDeserializer(BaseParams.Time.class, new TimeDeserializer());

            this.addSerializer(BaseParams.Extras.class, new ExtrasSerializer());
            this.addDeserializer(BaseParams.Extras.class, new ExtrasDeserializer());
*/
            this.addSerializer(DateTime.class, new DateTimeSerializer());
            this.addDeserializer(DateTime.class, new DateTimeDeserializer());
        }

    }

    public static class GeofencesCustomVisibilityChecker extends VisibilityChecker.Std{

        public GeofencesCustomVisibilityChecker(JsonAutoDetect ann) {
            super(ann);
        }

        public GeofencesCustomVisibilityChecker (JsonAutoDetect.Visibility v){
            super(v);

            this.withVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE);
        }

/*        @Override
        public boolean isFieldVisible(Field f) {
            if (f.isAnnotationPresent(BaseParams.NoParam.class) || f.isAnnotationPresent(BaseParams.NoSerialize.class) ){
                return false;
            }
            else if (ReflectionUtils.typeIsOfClass(f.getType(),BaseParams.Extras.class) || ReflectionUtils.typeIsOfClass(f.getType(), BaseParams.StringValue.class))  {
                return true;
            }
            return super.isFieldVisible(f);
        }

        @Override
        public boolean isGetterVisible(Method m) {
            if (m.isAnnotationPresent(BaseParams.ParamsInfoAnnotation.class) || m.isAnnotationPresent(BaseParams.NoSerialize.class))
            {
                return false;
            }
            return super.isGetterVisible(m);
        }*/
    }

}
