/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodriguez2002cu on 12/04/2016.
 */
public class Targets {

    public static final String APPLICATIONS = "application";
    public static final String TOKENS = "tokens";
    public static final String AUTHORIZATION = "authorization";
    public static final String ACCOUNT = "account";
    public static final String USERS = "users";
    public static final String LOCATION = "location";

    private static List<String > targets = new ArrayList<>();

    static {
        targets.add(APPLICATIONS);
        targets.add(TOKENS);
        targets.add(AUTHORIZATION);
        targets.add(ACCOUNT);
        targets.add(USERS);
        targets.add(LOCATION);
    }

    public static boolean isTargetRegistered(String targetToCheck){
        for (String target : targets) {
            if (target.equalsIgnoreCase(targetToCheck)){
                return true;
            }
        }
        return false;
    }

}
