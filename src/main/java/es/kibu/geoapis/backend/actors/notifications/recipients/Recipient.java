/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.notifications.recipients;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lrodriguez2002cu on 25/04/2016.
 */
public interface Recipient extends Serializable{

    //Whats the target?
    String getTarget();

    String getPlatform();

    String getHardware();

    String getName();

    String getVersion();

    List<String> getCategories();

}
